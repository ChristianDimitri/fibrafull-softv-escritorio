Imports System.Data.SqlClient
Public Class BrwPolizas
    Private Sub busca(ByVal op As Integer)
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Select Case op
            Case 0
                Me.Busca_PolizaTableAdapter.Connection = con
                Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, "", "")
            Case 1
                Me.Busca_PolizaTableAdapter.Connection = con
                Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, CLng(Me.TextBox1.Text), "", "")
            Case 2
                Me.Busca_PolizaTableAdapter.Connection = con
                Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, Me.TextBox3.Text, "")
            Case 3
                Me.Busca_PolizaTableAdapter.Connection = con
                Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, "", Me.TextBox4.Text)
        End Select

    End Sub

    Private Sub BrwPolizas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndactualizapoiza = True Then
            Locbndactualizapoiza = False
            busca(0)
        End If
    End Sub



    Private Sub BrwPolizas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        busca(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        busca(1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busca(2)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        busca(3)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

 

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub

    

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress

        If Asc(e.KeyChar) = 13 Then
            busca(3)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        LocopPoliza = "C"
        LocGloClv_poliza = CLng(Me.Clv_calleLabel2.Text)
        FrmPoliza.Show()

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim COn As New SqlConnection(MiConexion)
        COn.Open()
        LocopPoliza = "N"
        Me.DameClv_Session_ServiciosTableAdapter.Connection = COn
        Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        COn.Close()
        LocbndPolizaCiudad = True
        FrmSelCiudad.Show()
    End Sub

    
    Private Sub BrwPolizas_PaddingChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PaddingChanged

    End Sub
End Class