﻿Public Class FrmRepPruebaInternet

    Private Sub FrmRepPruebaInternet_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        dtpFechaIni.Value = DateTime.Today
        dtpFechaFin.Value = DateTime.Today

    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        eFechaIni = dtpFechaIni.Value
        eFechaFin = dtpFechaFin.Value
        eOpVentas = 95
        FrmImprimirComision.Show()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class