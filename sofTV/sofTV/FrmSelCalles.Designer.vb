<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCalles
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.MuestraCallesAsociadasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo
        Me.Muestra_Calles_AsociadasTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Calles_AsociadasTableAdapter
        Me.Inserta_Sel_CallesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Sel_CallesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Sel_CallesTableAdapter
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.InsertaTOSelecciona_CallesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_CallesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_CallesTableAdapter
        Me.InsertaTOSelecciona_CallesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_CallesTmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_CallesTmpTableAdapter
        Me.Insertauno_Seleccion_CallesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_CallesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_CallesTableAdapter
        Me.Insertauno_Seleccion_CallestmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_CallestmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_CallestmpTableAdapter
        Me.MuestraSeleccion_CalleCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSeleccion_CalleCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_CalleCONSULTATableAdapter
        Me.MuestraSeleccion_CallesTmpNUEVOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSeleccion_CallesTmpNUEVOTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_CallesTmpNUEVOTableAdapter
        Me.MuestraSeleccionaCalleTmpCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_CalleTmpCONSULTATableAdapter
        CType(Me.MuestraCallesAsociadasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Sel_CallesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_CallesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_CallesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_CallesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_CallestmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccion_CalleCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccion_CallesTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaCalleTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(364, 9)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(169, 15)
        Me.CMBLabel2.TabIndex = 64
        Me.CMBLabel2.Text = "Calle(s) Seleccionada(s):"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(167, 15)
        Me.CMBLabel1.TabIndex = 63
        Me.CMBLabel1.Text = "Seleccione la(s) calle(s):"
        '
        'ListBox3
        '
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.Location = New System.Drawing.Point(11, 260)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.ScrollAlwaysVisible = True
        Me.ListBox3.Size = New System.Drawing.Size(202, 43)
        Me.ListBox3.TabIndex = 62
        Me.ListBox3.TabStop = False
        Me.ListBox3.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(471, 267)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 59
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(292, 267)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 58
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(258, 154)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 57
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(258, 125)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 56
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(258, 57)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 55
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(258, 28)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 54
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSeleccion_CalleCONSULTABindingSource
        Me.ListBox2.DisplayMember = "nombre"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(365, 28)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(209, 225)
        Me.ListBox2.TabIndex = 61
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_calle"
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSeleccionaCalleTmpCONSULTABindingSource
        Me.ListBox1.DisplayMember = "nombre"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(11, 28)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(202, 225)
        Me.ListBox1.TabIndex = 60
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_calle"
        '
        'MuestraCallesAsociadasBindingSource
        '
        Me.MuestraCallesAsociadasBindingSource.DataMember = "Muestra_Calles_Asociadas"
        Me.MuestraCallesAsociadasBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_Calles_AsociadasTableAdapter
        '
        Me.Muestra_Calles_AsociadasTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Sel_CallesBindingSource
        '
        Me.Inserta_Sel_CallesBindingSource.DataMember = "Inserta_Sel_Calles"
        Me.Inserta_Sel_CallesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Sel_CallesTableAdapter
        '
        Me.Inserta_Sel_CallesTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'InsertaTOSelecciona_CallesBindingSource
        '
        Me.InsertaTOSelecciona_CallesBindingSource.DataMember = "InsertaTOSelecciona_Calles"
        Me.InsertaTOSelecciona_CallesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_CallesTableAdapter
        '
        Me.InsertaTOSelecciona_CallesTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSelecciona_CallesTmpBindingSource
        '
        Me.InsertaTOSelecciona_CallesTmpBindingSource.DataMember = "InsertaTOSelecciona_CallesTmp"
        Me.InsertaTOSelecciona_CallesTmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_CallesTmpTableAdapter
        '
        Me.InsertaTOSelecciona_CallesTmpTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_CallesBindingSource
        '
        Me.Insertauno_Seleccion_CallesBindingSource.DataMember = "Insertauno_Seleccion_Calles"
        Me.Insertauno_Seleccion_CallesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_CallesTableAdapter
        '
        Me.Insertauno_Seleccion_CallesTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_CallestmpBindingSource
        '
        Me.Insertauno_Seleccion_CallestmpBindingSource.DataMember = "Insertauno_Seleccion_Callestmp"
        Me.Insertauno_Seleccion_CallestmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_CallestmpTableAdapter
        '
        Me.Insertauno_Seleccion_CallestmpTableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccion_CalleCONSULTABindingSource
        '
        Me.MuestraSeleccion_CalleCONSULTABindingSource.DataMember = "MuestraSeleccion_CalleCONSULTA"
        Me.MuestraSeleccion_CalleCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSeleccion_CalleCONSULTATableAdapter
        '
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccion_CallesTmpNUEVOBindingSource
        '
        Me.MuestraSeleccion_CallesTmpNUEVOBindingSource.DataMember = "MuestraSeleccion_CallesTmpNUEVO"
        Me.MuestraSeleccion_CallesTmpNUEVOBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSeleccion_CallesTmpNUEVOTableAdapter
        '
        Me.MuestraSeleccion_CallesTmpNUEVOTableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccionaCalleTmpCONSULTABindingSource
        '
        Me.MuestraSeleccionaCalleTmpCONSULTABindingSource.DataMember = "MuestraSelecciona_CalleTmpCONSULTA"
        Me.MuestraSeleccionaCalleTmpCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSelecciona_CalleTmpCONSULTATableAdapter
        '
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.ClearBeforeFill = True
        '
        'FrmSelCalles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(626, 326)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ListBox3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmSelCalles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Calles."
        CType(Me.MuestraCallesAsociadasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Sel_CallesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_CallesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_CallesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_CallesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_CallestmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccion_CalleCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccion_CallesTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaCalleTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MuestraCallesAsociadasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Muestra_Calles_AsociadasTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Calles_AsociadasTableAdapter
    Friend WithEvents Inserta_Sel_CallesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Sel_CallesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Sel_CallesTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents InsertaTOSelecciona_CallesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_CallesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_CallesTableAdapter
    Friend WithEvents InsertaTOSelecciona_CallesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_CallesTmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_CallesTmpTableAdapter
    Friend WithEvents Insertauno_Seleccion_CallesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_CallesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_CallesTableAdapter
    Friend WithEvents Insertauno_Seleccion_CallestmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_CallestmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_CallestmpTableAdapter
    Friend WithEvents MuestraSeleccion_CalleCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccion_CalleCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_CalleCONSULTATableAdapter
    Friend WithEvents MuestraSeleccion_CallesTmpNUEVOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccion_CallesTmpNUEVOTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_CallesTmpNUEVOTableAdapter
    Friend WithEvents MuestraSeleccionaCalleTmpCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_CalleTmpCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_CalleTmpCONSULTATableAdapter
End Class
