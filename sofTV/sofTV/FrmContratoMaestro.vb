Imports System.Data.SqlClient
Public Class FrmContratoMaestro

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmSubContrato.Show()
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConContratoMaestroDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eGloContratoAux = Me.CONTRATOLabel1.Text
            FrmSubContrato.Show()
        Else
            MsgBox("No Existen Contratos Maestros a Consultar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConContratoMaestroDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eGloContratoAux = Me.CONTRATOLabel1.Text
            FrmSubContrato.Show()
        Else
            MsgBox("No Existen Contratos Maestros a Modificar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub FrmContratoMaestro_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConContratoMaestroTableAdapter.Connection = CON
        Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", "", "", "", 3)
        CON.Close()
    End Sub

    Private Sub FrmContratoMaestro_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.ConContratoMaestroTableAdapter.Connection = CON
        Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", "", "", "", 3)
        CON.Close()

        UspDesactivaBotones(Me, Me.Name)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.TextBox1.Text) = True Then
            Me.ConContratoMaestroTableAdapter.Connection = CON
            Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, Me.TextBox1.Text, 0, "", "", "", "", 0)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                Me.ConContratoMaestroTableAdapter.Connection = CON
                Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, Me.TextBox1.Text, 0, "", "", "", "", 0)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.TextBox2.Text.Length > 0 Then
            Me.ConContratoMaestroTableAdapter.Connection = CON
            Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, Me.TextBox2.Text, "", "", "", 1)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox2.Text.Length > 0 Then
                Me.ConContratoMaestroTableAdapter.Connection = CON
                Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, Me.TextBox2.Text, "", "", "", 1)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        Me.ConContratoMaestroTableAdapter.Connection = CON
        Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Me.ConContratoMaestroTableAdapter.Connection = CON
            Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Me.ConContratoMaestroTableAdapter.Connection = CON
            Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Me.ConContratoMaestroTableAdapter.Connection = CON
            Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        End If
        CON.Close()
    End Sub


End Class