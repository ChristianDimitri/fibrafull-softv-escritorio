<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGraficas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.Dame_clv_session_ReportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_ReportesTableAdapter = New sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter()
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.ConVentasVendedoresProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConVentasVendedoresProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConVentasVendedoresProTableAdapter()
        Me.NombreListBox = New System.Windows.Forms.ListBox()
        Me.ConVentasVendedoresTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConVentasVendedoresTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConVentasVendedoresTmpTableAdapter()
        Me.NombreListBox1 = New System.Windows.Forms.ListBox()
        Me.InsertarVendedorTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarVendedorTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarVendedorTmpTableAdapter()
        Me.BorrarVendedorTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarVendedorTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarVendedorTmpTableAdapter()
        Me.ConServiciosProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConServiciosProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConServiciosProTableAdapter()
        Me.ConServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConServiciosTmpTableAdapter()
        Me.InsertarServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarServiciosTmpTableAdapter()
        Me.BorrarServiciosTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarServiciosTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarServiciosTmpTableAdapter()
        Me.ConSucursalesProBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSucursalesProTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSucursalesProTableAdapter()
        Me.ConSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.ConSucursalesTmpTableAdapter()
        Me.InsertarSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertarSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.InsertarSucursalesTmpTableAdapter()
        Me.BorrarSucursalesTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorrarSucursalesTmpTableAdapter = New sofTV.DataSetEric2TableAdapters.BorrarSucursalesTmpTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.NombreListBox3 = New System.Windows.Forms.ListBox()
        Me.NombreListBox2 = New System.Windows.Forms.ListBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.DescripcionListBox1 = New System.Windows.Forms.ListBox()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.DescripcionListBox = New System.Windows.Forms.ListBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConVentasVendedoresProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConVentasVendedoresTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSucursalesProBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorrarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clv_session_ReportesBindingSource
        '
        Me.Dame_clv_session_ReportesBindingSource.DataMember = "Dame_clv_session_Reportes"
        Me.Dame_clv_session_ReportesBindingSource.DataSource = Me.DataSetEric2
        '
        'Dame_clv_session_ReportesTableAdapter
        '
        Me.Dame_clv_session_ReportesTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(24, 29)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(305, 24)
        Me.ConceptoComboBox.TabIndex = 0
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'ConVentasVendedoresProBindingSource
        '
        Me.ConVentasVendedoresProBindingSource.DataMember = "ConVentasVendedoresPro"
        Me.ConVentasVendedoresProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConVentasVendedoresProTableAdapter
        '
        Me.ConVentasVendedoresProTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox
        '
        Me.NombreListBox.DataSource = Me.ConVentasVendedoresProBindingSource
        Me.NombreListBox.DisplayMember = "Nombre"
        Me.NombreListBox.FormattingEnabled = True
        Me.NombreListBox.ItemHeight = 16
        Me.NombreListBox.Location = New System.Drawing.Point(13, 22)
        Me.NombreListBox.Name = "NombreListBox"
        Me.NombreListBox.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox.TabIndex = 4
        Me.NombreListBox.ValueMember = "Clv_Vendedor"
        '
        'ConVentasVendedoresTmpBindingSource
        '
        Me.ConVentasVendedoresTmpBindingSource.DataMember = "ConVentasVendedoresTmp"
        Me.ConVentasVendedoresTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConVentasVendedoresTmpTableAdapter
        '
        Me.ConVentasVendedoresTmpTableAdapter.ClearBeforeFill = True
        '
        'NombreListBox1
        '
        Me.NombreListBox1.DataSource = Me.ConVentasVendedoresTmpBindingSource
        Me.NombreListBox1.DisplayMember = "Nombre"
        Me.NombreListBox1.FormattingEnabled = True
        Me.NombreListBox1.ItemHeight = 16
        Me.NombreListBox1.Location = New System.Drawing.Point(415, 19)
        Me.NombreListBox1.Name = "NombreListBox1"
        Me.NombreListBox1.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox1.TabIndex = 6
        Me.NombreListBox1.ValueMember = "Clv_Vendedor"
        '
        'InsertarVendedorTmpBindingSource
        '
        Me.InsertarVendedorTmpBindingSource.DataMember = "InsertarVendedorTmp"
        Me.InsertarVendedorTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarVendedorTmpTableAdapter
        '
        Me.InsertarVendedorTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarVendedorTmpBindingSource
        '
        Me.BorrarVendedorTmpBindingSource.DataMember = "BorrarVendedorTmp"
        Me.BorrarVendedorTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarVendedorTmpTableAdapter
        '
        Me.BorrarVendedorTmpTableAdapter.ClearBeforeFill = True
        '
        'ConServiciosProBindingSource
        '
        Me.ConServiciosProBindingSource.DataMember = "ConServiciosPro"
        Me.ConServiciosProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConServiciosProTableAdapter
        '
        Me.ConServiciosProTableAdapter.ClearBeforeFill = True
        '
        'ConServiciosTmpBindingSource
        '
        Me.ConServiciosTmpBindingSource.DataMember = "ConServiciosTmp"
        Me.ConServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConServiciosTmpTableAdapter
        '
        Me.ConServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'InsertarServiciosTmpBindingSource
        '
        Me.InsertarServiciosTmpBindingSource.DataMember = "InsertarServiciosTmp"
        Me.InsertarServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarServiciosTmpTableAdapter
        '
        Me.InsertarServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarServiciosTmpBindingSource
        '
        Me.BorrarServiciosTmpBindingSource.DataMember = "BorrarServiciosTmp"
        Me.BorrarServiciosTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarServiciosTmpTableAdapter
        '
        Me.BorrarServiciosTmpTableAdapter.ClearBeforeFill = True
        '
        'ConSucursalesProBindingSource
        '
        Me.ConSucursalesProBindingSource.DataMember = "ConSucursalesPro"
        Me.ConSucursalesProBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSucursalesProTableAdapter
        '
        Me.ConSucursalesProTableAdapter.ClearBeforeFill = True
        '
        'ConSucursalesTmpBindingSource
        '
        Me.ConSucursalesTmpBindingSource.DataMember = "ConSucursalesTmp"
        Me.ConSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'ConSucursalesTmpTableAdapter
        '
        Me.ConSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'InsertarSucursalesTmpBindingSource
        '
        Me.InsertarSucursalesTmpBindingSource.DataMember = "InsertarSucursalesTmp"
        Me.InsertarSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'InsertarSucursalesTmpTableAdapter
        '
        Me.InsertarSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'BorrarSucursalesTmpBindingSource
        '
        Me.BorrarSucursalesTmpBindingSource.DataMember = "BorrarSucursalesTmp"
        Me.BorrarSucursalesTmpBindingSource.DataSource = Me.DataSetEric2
        '
        'BorrarSucursalesTmpTableAdapter
        '
        Me.BorrarSucursalesTmpTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(307, 104)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(307, 133)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(307, 206)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 17
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(307, 235)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 18
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(308, 110)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 19
        Me.Button5.Text = ">"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(308, 139)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 20
        Me.Button6.Text = ">>"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(308, 201)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 21
        Me.Button7.Text = "<"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(308, 230)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 22
        Me.Button8.Text = "<<"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(39, 29)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(104, 20)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Sucursales"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(182, 29)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(147, 20)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "Depto. de Ventas"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'NombreListBox3
        '
        Me.NombreListBox3.DataSource = Me.ConSucursalesTmpBindingSource
        Me.NombreListBox3.DisplayMember = "Nombre"
        Me.NombreListBox3.FormattingEnabled = True
        Me.NombreListBox3.ItemHeight = 16
        Me.NombreListBox3.Location = New System.Drawing.Point(407, 24)
        Me.NombreListBox3.Name = "NombreListBox3"
        Me.NombreListBox3.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox3.TabIndex = 18
        Me.NombreListBox3.ValueMember = "Clv_Sucursal"
        '
        'NombreListBox2
        '
        Me.NombreListBox2.DataSource = Me.ConSucursalesProBindingSource
        Me.NombreListBox2.DisplayMember = "Nombre"
        Me.NombreListBox2.FormattingEnabled = True
        Me.NombreListBox2.ItemHeight = 16
        Me.NombreListBox2.Location = New System.Drawing.Point(19, 24)
        Me.NombreListBox2.Name = "NombreListBox2"
        Me.NombreListBox2.Size = New System.Drawing.Size(258, 356)
        Me.NombreListBox2.TabIndex = 17
        Me.NombreListBox2.ValueMember = "Clv_Sucursal"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Enabled = False
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 176)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(809, 479)
        Me.TabControl1.TabIndex = 5
        Me.TabControl1.TabStop = False
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.ForeColor = System.Drawing.Color.Black
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(801, 450)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Sucursales / Vendedores"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.NombreListBox2)
        Me.Panel2.Controls.Add(Me.NombreListBox3)
        Me.Panel2.Controls.Add(Me.Button6)
        Me.Panel2.Controls.Add(Me.Button5)
        Me.Panel2.Controls.Add(Me.Button7)
        Me.Panel2.Controls.Add(Me.Button8)
        Me.Panel2.Location = New System.Drawing.Point(40, 33)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(716, 401)
        Me.Panel2.TabIndex = 32
        Me.Panel2.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.NombreListBox1)
        Me.Panel1.Controls.Add(Me.NombreListBox)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Location = New System.Drawing.Point(50, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(693, 391)
        Me.Panel1.TabIndex = 31
        Me.Panel1.Visible = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(801, 450)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Servicios"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.DescripcionListBox1)
        Me.Panel3.Controls.Add(Me.Button12)
        Me.Panel3.Controls.Add(Me.Button9)
        Me.Panel3.Controls.Add(Me.Button11)
        Me.Panel3.Controls.Add(Me.Button10)
        Me.Panel3.Controls.Add(Me.DescripcionListBox)
        Me.Panel3.Location = New System.Drawing.Point(52, 52)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(659, 361)
        Me.Panel3.TabIndex = 27
        '
        'DescripcionListBox1
        '
        Me.DescripcionListBox1.DataSource = Me.ConServiciosTmpBindingSource
        Me.DescripcionListBox1.DisplayMember = "Descripcion"
        Me.DescripcionListBox1.FormattingEnabled = True
        Me.DescripcionListBox1.ItemHeight = 16
        Me.DescripcionListBox1.Location = New System.Drawing.Point(398, 3)
        Me.DescripcionListBox1.Name = "DescripcionListBox1"
        Me.DescripcionListBox1.Size = New System.Drawing.Size(258, 356)
        Me.DescripcionListBox1.TabIndex = 16
        Me.DescripcionListBox1.TabStop = False
        Me.DescripcionListBox1.ValueMember = "Clv_Servicio"
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(294, 209)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(75, 23)
        Me.Button12.TabIndex = 26
        Me.Button12.TabStop = False
        Me.Button12.Text = "<<"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(294, 90)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 23
        Me.Button9.TabStop = False
        Me.Button9.Text = ">"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(294, 180)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(75, 23)
        Me.Button11.TabIndex = 25
        Me.Button11.TabStop = False
        Me.Button11.Text = "<"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(294, 119)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 24
        Me.Button10.TabStop = False
        Me.Button10.Text = ">>"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'DescripcionListBox
        '
        Me.DescripcionListBox.DataSource = Me.ConServiciosProBindingSource
        Me.DescripcionListBox.DisplayMember = "Descripcion"
        Me.DescripcionListBox.FormattingEnabled = True
        Me.DescripcionListBox.ItemHeight = 16
        Me.DescripcionListBox.Location = New System.Drawing.Point(3, 3)
        Me.DescripcionListBox.Name = "DescripcionListBox"
        Me.DescripcionListBox.Size = New System.Drawing.Size(258, 356)
        Me.DescripcionListBox.TabIndex = 15
        Me.DescripcionListBox.TabStop = False
        Me.DescripcionListBox.ValueMember = "Clv_Servicio"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(61, 27)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(134, 22)
        Me.DateTimePicker1.TabIndex = 0
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(245, 27)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(134, 22)
        Me.DateTimePicker2.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(393, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(413, 66)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Rango de Fechas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 16)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Del"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(217, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(22, 16)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Al"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ConceptoComboBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(17, 93)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(353, 66)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Servicio"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CheckBox2)
        Me.GroupBox3.Controls.Add(Me.CheckBox1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(17, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(353, 66)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Reportes"
        '
        'Button13
        '
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.Location = New System.Drawing.Point(550, 113)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(136, 36)
        Me.Button13.TabIndex = 4
        Me.Button13.Text = "&GRAFICAR"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmGraficas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(831, 664)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button13)
        Me.Name = "FrmGraficas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gráficas de Ventas"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConVentasVendedoresProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConVentasVendedoresTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarVendedorTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarServiciosTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSucursalesProBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorrarSucursalesTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents Dame_clv_session_ReportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_ReportesTableAdapter As sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConVentasVendedoresProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConVentasVendedoresProTableAdapter As sofTV.DataSetEric2TableAdapters.ConVentasVendedoresProTableAdapter
    Friend WithEvents NombreListBox As System.Windows.Forms.ListBox
    Friend WithEvents ConVentasVendedoresTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConVentasVendedoresTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConVentasVendedoresTmpTableAdapter
    Friend WithEvents NombreListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents InsertarVendedorTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarVendedorTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarVendedorTmpTableAdapter
    Friend WithEvents BorrarVendedorTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarVendedorTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarVendedorTmpTableAdapter
    Friend WithEvents ConServiciosProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosProTableAdapter As sofTV.DataSetEric2TableAdapters.ConServiciosProTableAdapter
    Friend WithEvents ConServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConServiciosTmpTableAdapter
    Friend WithEvents InsertarServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarServiciosTmpTableAdapter
    Friend WithEvents BorrarServiciosTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarServiciosTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarServiciosTmpTableAdapter
    Friend WithEvents ConSucursalesProBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSucursalesProTableAdapter As sofTV.DataSetEric2TableAdapters.ConSucursalesProTableAdapter
    Friend WithEvents ConSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.ConSucursalesTmpTableAdapter
    Friend WithEvents InsertarSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertarSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.InsertarSucursalesTmpTableAdapter
    Friend WithEvents BorrarSucursalesTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorrarSucursalesTmpTableAdapter As sofTV.DataSetEric2TableAdapters.BorrarSucursalesTmpTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents NombreListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents NombreListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents DescripcionListBox As System.Windows.Forms.ListBox
    Friend WithEvents DescripcionListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
