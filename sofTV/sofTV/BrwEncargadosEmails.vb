Imports System.Data.SqlClient

Public Class BrwEncargadosEmails
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmEncargadosEmails.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConEncargadosEmailsDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_Encargado = Me.Clv_EncargadoTextBox.Text
            FrmEncargadosEmails.Show()
        Else
            MsgBox("No existen Registros a Modificar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub BrwEncargadosEmails_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConEncargadosEmailsTableAdapter.Connection = CON
        Me.ConEncargadosEmailsTableAdapter.Fill(Me.DataSetEric.ConEncargadosEmails, 0, 0)
        CON.Close()
    End Sub

    Private Sub BrwEncargadosEmails_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.ConEncargadosEmailsTableAdapter.Connection = CON
        Me.ConEncargadosEmailsTableAdapter.Fill(Me.DataSetEric.ConEncargadosEmails, 0, 0)
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
End Class