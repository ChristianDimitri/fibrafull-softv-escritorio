﻿Public Class FrmtblSeleccionColonia

    Private Sub MUESTRAtblSeleccionColonia(ByVal Clv_Session As Integer, ByVal Op As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        dTable = BaseII.ConsultaDT("MUESTRAtblSeleccionColonia")
        If Op = 0 Then dgvIzq.DataSource = dTable
        If Op = 1 Then dgvDer.DataSource = dTable
    End Sub

    Private Sub INSERTAtblSeleccionColonia(ByVal Clv_Session As Integer, ByVal Op As Integer, Clv_Colonia As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.Inserta("INSERTAtblSeleccionColonia")
    End Sub

    Private Sub ELIMINAtblSeleccionColonia(ByVal Clv_Session As Integer, ByVal Op As Integer, Clv_Colonia As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.Inserta("ELIMINAtblSeleccionColonia")
    End Sub

    Private Sub DameClv_Session()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        eClv_Session = CLng(BaseII.dicoPar("@Clv_Session").ToString)
    End Sub

    Private Sub FrmtblSeleccionColonia_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        'DameClv_Session()
        ELIMINAtblSeleccionColonia(eClv_Session, 1, 0)
        MUESTRAtblSeleccionColonia(eClv_Session, 0)
        MUESTRAtblSeleccionColonia(eClv_Session, 1)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        If dgvIzq.SelectedCells(0).Value = 0 Then
            Exit Sub
        End If
        INSERTAtblSeleccionColonia(eClv_Session, 0, dgvIzq.SelectedCells(0).Value)
        MUESTRAtblSeleccionColonia(eClv_Session, 0)
        MUESTRAtblSeleccionColonia(eClv_Session, 1)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        INSERTAtblSeleccionColonia(eClv_Session, 1, 0)
        MUESTRAtblSeleccionColonia(eClv_Session, 0)
        MUESTRAtblSeleccionColonia(eClv_Session, 1)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINAtblSeleccionColonia(eClv_Session, 0, dgvDer.SelectedCells(0).Value)
        MUESTRAtblSeleccionColonia(eClv_Session, 0)
        MUESTRAtblSeleccionColonia(eClv_Session, 1)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINAtblSeleccionColonia(eClv_Session, 1, dgvDer.SelectedCells(0).Value)
        MUESTRAtblSeleccionColonia(eClv_Session, 0)
        MUESTRAtblSeleccionColonia(eClv_Session, 1)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona una Ciudad.")
            Exit Sub
        End If

        GlobndClv_Colonia = True

        If dgvDer.RowCount = 1 Then
            GLONOMCOLONIA = dgvDer.SelectedCells(1).Value
        Else
            GLONOMCOLONIA = "SELECCIÓN MÚLTIPLE"
        End If

        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

End Class