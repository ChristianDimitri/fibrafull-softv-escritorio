
Imports System.IO
Imports System.Data.SqlClient
Public Class FrmServicioPPE2

    Dim bytesImg() As Byte

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)

            If Me.Clv_TxtTextBox.Text.Length > 0 Then

                If Me.DescripcionTextBox.Text.Length > 0 Then

                    If IsNumeric(Me.PrecioTextBox.Text.Length) = True Then

                        If IsNumeric(Me.HitsTextBox.Text) = False Then
                            Me.HitsTextBox.Text = 0
                        End If

                        If Me.Clv_TxtCNRTextBox.Text.Length = 0 Then
                            MsgBox("Captura el Canal.", , "Atención")
                            Exit Sub
                        End If
                        If eOpcion = "N" Then
                            CON.Open()
                            bytesImg = Image2Bytes(Me.ImagenPictureBox.Image)
                            Me.CONSERVICIOSTableAdapter.Connection = CON
                            Me.CONSERVICIOSTableAdapter.Insert(4, Me.DescripcionTextBox.Text, Me.Clv_TxtTextBox.Text, Me.AplicanComCheckBox.Checked, False, Me.PrecioTextBox.Text, Me.Genera_OrdenCheckBox.Checked, False, eClv_Servicio)
                            Me.ConServiciosPPETableAdapter.Connection = CON
                            Me.ConServiciosPPETableAdapter.Insert(eClv_Servicio, 0, Me.ClasificacionTextBox.Text, bytesImg)
                            Me.DameClv_PPETableAdapter.Connection = CON
                            Me.DameClv_PPETableAdapter.Fill(Me.DataSetEricPPE.DameClv_PPE, eClv_Servicio)
                            If IsNumeric(Me.Clv_PPETextBox.Text) = True Then
                                eClv_PPE = Me.Clv_PPETextBox.Text
                            Else
                                eClv_PPE = 0
                            End If
                            Me.ConRElPPECNRTableAdapter.Connection = CON
                            Me.ConRElPPECNRTableAdapter.Insert(eClv_PPE, Me.Clv_TxtCNRTextBox.Text)
                            eOpcion = "M"
                            'MsgBox(mensaje5)
                            CON.Close()

                        End If
                        If eOpcion = "M" Then
                            CON.Open()
                            bytesImg = Image2Bytes(Me.ImagenPictureBox.Image)
                            Me.Validate()
                            Me.CONSERVICIOSBindingSource.EndEdit()
                            Me.CONSERVICIOSTableAdapter.Connection = CON
                            Me.CONSERVICIOSTableAdapter.Update(eClv_Servicio, 4, Me.DescripcionTextBox.Text, Me.Clv_TxtTextBox.Text, Me.AplicanComCheckBox.Checked, False, Me.PrecioTextBox.Text, Me.Genera_OrdenCheckBox.Checked, False)
                            Me.ConServiciosPPETableAdapter.Connection = CON
                            Me.ConServiciosPPETableAdapter.Update(eClv_PPE, eClv_Servicio, Me.HitsTextBox.Text, Me.ClasificacionTextBox.Text, bytesImg)
                            Me.ConRElPPECNRTableAdapter.Connection = CON
                            Me.ConRElPPECNRTableAdapter.Insert(eClv_PPE, Me.Clv_TxtCNRTextBox.Text)
                            'MsgBox(mensaje5)
                            CON.Close()

                        End If


                        If eOpcion <> "C" Then
                            If Me.Genera_OrdenCheckBox.Checked = True Then
                                If Me.DescripcionComboBox.Text.Length > 0 Then
                                    GuardaRel_Trabajos_NoCobroMensual()
                                    MsgBox(mensaje5)
                                    Me.Panel4.Enabled = True
                                    Me.GroupBox1.Enabled = True
                                    'Me.Close()
                                Else
                                    MsgBox("Captura el Trabajo.")
                                End If
                            Else
                                BorRel_Trabajos_NoCobroMensual()
                                MsgBox(mensaje5)
                                Me.Panel4.Enabled = True
                                Me.GroupBox1.Enabled = True
                                'Me.Close()
                            End If
                        End If
                    Else
                        MsgBox("Captura el Precio.", , "Atención")
                    End If
                Else
                    MsgBox("Captura la Descripción.", , "Atención")
                End If
            Else
                MsgBox("Captura la Clave.", , "Atención")
            End If

            GloBnd = True

            'Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa3.txt")
            'End Using

        Catch
            MsgBox("Ya Existe esa Clave. Intente con otra.", , "Atención")
        End Try


    End Sub

    Private Sub GuardaRel_Trabajos_NoCobroMensual()
        Try
            Dim CON2 As New SqlConnection(MiConexion)

            If IsNumeric(eClv_Servicio) = True Then
                If Me.DescripcionComboBox.Text.Length > 0 And IsNumeric(Me.DescripcionComboBox.SelectedValue) = True Then
                    CON2.Open()
                    Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter.Connection = CON2
                    Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEricPPE.GUARDARel_Trabajos_NoCobroMensual, eClv_Servicio, Me.DescripcionComboBox.SelectedValue)
                    CON2.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BorRel_Trabajos_NoCobroMensual()
        Try
            Dim CON3 As New SqlConnection(MiConexion)

            If IsNumeric(eClv_Servicio) = True Then
                CON3.Open()
                Me.BORRel_Trabajos_NoCobroMensualTableAdapter.Connection = CON3
                Me.BORRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEricPPE.BORRel_Trabajos_NoCobroMensual, eClv_Servicio)
                CON3.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            Dim CON4 As New SqlConnection(MiConexion)
            CON4.Open()
            Dim error1 As Integer = 0
            Dim Mensaje As String = ""

            Me.Valida_borra_servicioTableAdapter.Connection = CON4
            Me.Valida_borra_servicioTableAdapter.Fill(Me.DataSetEricPPE.Valida_borra_servicio, eClv_Servicio, error1, Mensaje)

            If error1 = 1 Then
                MsgBox(Mensaje, MsgBoxStyle.Information)
                Exit Sub
            End If


            Me.CONSERVICIOSTableAdapter.Connection = CON4
            Me.CONSERVICIOSTableAdapter.Delete(eClv_Servicio)

            Me.ConServiciosPPETableAdapter.Connection = CON4
            Me.ConServiciosPPETableAdapter.Delete(eClv_PPE)

            Me.ConRElPPECNRTableAdapter.Connection = CON4
            Me.ConRElPPECNRTableAdapter.Delete(eClv_PPE)
            MsgBox(mensaje6)

            GloBnd = True

            'Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa3.txt")
            'End Using
            CON4.Close()
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Genera_OrdenCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Genera_OrdenCheckBox.CheckedChanged
        If Me.Genera_OrdenCheckBox.Checked = True Then
            Me.DescripcionComboBox.Enabled = True
            Me.Panel2.Enabled = True
        Else
            Me.DescripcionComboBox.Enabled = False
            Me.Panel2.Enabled = False
        End If
    End Sub

    Private Sub FrmServicioPPE_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraProgramacionPPETableAdapter.Connection = CON
        Me.MuestraProgramacionPPETableAdapter.Fill(Me.DataSetEricPPE.MuestraProgramacionPPE, 0, Me.Clv_TxtTextBox.Text, Today, 0)
        CON.Close()
    End Sub

    Private Sub FrmServicioPPE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, "FrmServicioPPE2")
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker1.MinDate = Today
        Me.DateTimePicker2.Value = Today
        Dim CON As New SqlConnection(MiConexion)

        If eOpcion = "N" Then
            CON.Open()
            Me.BindingNavigatorDeleteItem.Enabled = False
            Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Connection = CON
            Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Fill(Me.DataSetEricPPE.MUESTRATRABAJOS_NOCOBROMENSUAL, 4, 0)
            CON.Close()
        End If

        If eOpcion = "C" Then
            CON.Open()
            Me.CONSERVICIOSTableAdapter.Connection = CON
            Me.CONSERVICIOSTableAdapter.Fill(Me.DataSetEricPPE.CONSERVICIOS, eClv_Servicio)
            Me.ConServiciosPPETableAdapter.Connection = CON
            Me.ConServiciosPPETableAdapter.Fill(Me.DataSetEricPPE.ConServiciosPPE, eClv_PPE)
            Me.ConRElPPECNRTableAdapter.Connection = CON
            Me.ConRElPPECNRTableAdapter.Fill(Me.DataSetEricPPE.ConRElPPECNR, eClv_PPE)
            Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Connection = CON
            Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Fill(Me.DataSetEricPPE.MUESTRATRABAJOS_NOCOBROMENSUAL, 4, 0)
            Me.Panel1.Enabled = False
            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = False
            Me.BindingNavigator1.Enabled = False
            Me.GroupBox1.Enabled = False
            Me.CONSERVICIOSBindingNavigator.Enabled = False
            If Me.Genera_OrdenCheckBox.Checked = True Then
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Connection = CON
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEricPPE.CONRel_Trabajos_NoCobroMensual, eClv_Servicio)
            End If
            Me.MuestraProgramacionPPETableAdapter.Connection = CON
            Me.MuestraProgramacionPPETableAdapter.Fill(Me.DataSetEricPPE.MuestraProgramacionPPE, 0, Me.Clv_TxtTextBox.Text, Today, 0)
            CON.Close()
        End If

        If eOpcion = "M" Then
            CON.Open()
            Me.CONSERVICIOSTableAdapter.Connection = CON
            Me.CONSERVICIOSTableAdapter.Fill(Me.DataSetEricPPE.CONSERVICIOS, eClv_Servicio)
            Me.ConServiciosPPETableAdapter.Connection = CON
            Me.ConServiciosPPETableAdapter.Fill(Me.DataSetEricPPE.ConServiciosPPE, eClv_PPE)
            Me.ConRElPPECNRTableAdapter.Connection = CON
            Me.ConRElPPECNRTableAdapter.Fill(Me.DataSetEricPPE.ConRElPPECNR, eClv_PPE)
            Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Connection = CON
            Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Fill(Me.DataSetEricPPE.MUESTRATRABAJOS_NOCOBROMENSUAL, 4, 0)
            If Me.Genera_OrdenCheckBox.Checked = True Then
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Connection = CON
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEricPPE.CONRel_Trabajos_NoCobroMensual, eClv_Servicio)
            End If
            Me.Panel4.Enabled = True
            Me.GroupBox1.Enabled = True
            Me.MuestraProgramacionPPETableAdapter.Connection = CON
            Me.MuestraProgramacionPPETableAdapter.Fill(Me.DataSetEricPPE.MuestraProgramacionPPE, 0, Me.Clv_TxtTextBox.Text, Today, 0)
            CON.Close()
        End If

        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        AsignaImagen(Me.ImagenPictureBox)
    End Sub

    Private Sub Panel4_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel4.Paint

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker1.Enabled = True
        Me.DateTimePicker1.MaxDate = "01/01/2100"
        Me.DateTimePicker2.Enabled = False
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker1.Enabled = True
        Me.DateTimePicker2.Value = Today
        Me.DateTimePicker2.Enabled = True
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim CON As New SqlConnection(MiConexion)

        If Me.RadioButton1.Checked = True Then
            CON.Open()
            Me.ValidaNueProgPPETableAdapter.Connection = CON
            Me.ValidaNueProgPPETableAdapter.Fill(Me.DataSetEricPPE.ValidaNueProgPPE, Me.Clv_TxtTextBox.Text, Me.DateTimePicker1.Value, Today, 0, eRespuesta)
            CON.Close()
            If eRespuesta = 0 Then
                CON.Open()
                Me.InsertaFechasProgPPETableAdapter.Connection = CON
                Me.InsertaFechasProgPPETableAdapter.Fill(Me.DataSetEricPPE.InsertaFechasProgPPE, Me.Clv_TxtTextBox.Text, Me.DateTimePicker1.Value, Today, 0)
                CON.Close()
                MsgBox(mensaje5)
            Else
                MsgBox("Ya Existe una Programacion con esa Fecha.", , "Atención")
            End If
        Else
            CON.Open()
            Me.ValidaNueProgPPETableAdapter.Connection = CON
            Me.ValidaNueProgPPETableAdapter.Fill(Me.DataSetEricPPE.ValidaNueProgPPE, Me.Clv_TxtTextBox.Text, Me.DateTimePicker1.Value, Me.DateTimePicker2.Value, 1, eRespuesta)
            CON.Close()
            If eRespuesta = 0 Then
                CON.Open()
                Me.InsertaFechasProgPPETableAdapter.Connection = CON
                Me.InsertaFechasProgPPETableAdapter.Fill(Me.DataSetEricPPE.InsertaFechasProgPPE, Me.Clv_TxtTextBox.Text, Me.DateTimePicker1.Value, Me.DateTimePicker2.Value, 1)
                CON.Close()
                MsgBox(mensaje5)
            Else
                MsgBox("Ya Existe una Programación dentro de ese Rango.", , "Atención")
            End If
        End If

        GloBnd = True

        'Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa3.txt")
        'End Using
        CON.Open()
        Me.MuestraProgramacionPPETableAdapter.Connection = CON
        Me.MuestraProgramacionPPETableAdapter.Fill(Me.DataSetEricPPE.MuestraProgramacionPPE, 0, Me.Clv_TxtTextBox.Text, Today, 0)
        CON.Close()

    End Sub


    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        If Me.RadioButton2.Checked = True Then
            Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
        End If
    End Sub

    Private Sub AsignaImagen(ByVal Picture As PictureBox)
        Try
            Dim ImageDialog As New OpenFileDialog
            ImageDialog.Multiselect = False
            ImageDialog.ShowDialog()
            Me.ImagenPictureBox.SizeMode = PictureBoxSizeMode.StretchImage
            Me.ImagenPictureBox.ImageLocation = ImageDialog.FileName
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Dim CON2 As New SqlConnection(MiConexion)

        If Me.MuestraProgramacionPPEDataGridView.RowCount > 0 Then
            CON2.Open()
            Me.ValidaBorProgPPETableAdapter.Connection = CON2
            Me.ValidaBorProgPPETableAdapter.Fill(Me.DataSetEricPPE.ValidaBorProgPPE, Me.Clv_PrograLabel1.Text, eRespuesta)
            CON2.Close()
            If eRespuesta = 0 Then
                CON2.Open()
                Me.BorraFechasProgPPETableAdapter.Connection = CON2
                Me.BorraFechasProgPPETableAdapter.Fill(Me.DataSetEricPPE.BorraFechasProgPPE, Me.Clv_PrograLabel1.Text)
                CON2.Close()

                GloBnd = True

                'Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa3.txt")
                'End Using
            Else
                MsgBox("La Programación ya ha sido Asignada.", , "Error")
            End If
        Else
            MsgBox("No Existe una Programación a Eliminar", , "Atención")
        End If

    End Sub

    Private Sub DescripcionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DescripcionTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(DescripcionTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub PrecioTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PrecioTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.PrecioTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub ToolStripButton1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Dim CON2 As New SqlConnection(MiConexion)

        If Me.MuestraProgramacionPPEDataGridView.RowCount > 0 Then
            CON2.Open()
            Me.ValidaBorProgPPETableAdapter.Connection = CON2
            Me.ValidaBorProgPPETableAdapter.Fill(Me.DataSetEricPPE.ValidaBorProgPPE, Me.Clv_PrograLabel1.Text, eRespuesta)
            CON2.Close()
            If eRespuesta = 0 Then
                CON2.Open()
                Me.BorraFechasProgPPETableAdapter.Connection = CON2
                Me.BorraFechasProgPPETableAdapter.Fill(Me.DataSetEricPPE.BorraFechasProgPPE, Me.Clv_PrograLabel1.Text)
                CON2.Close()
                Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa3.txt")
                End Using
            Else
                MsgBox("La Programación ya ha sido Asignada.", , "Error")
            End If
        Else
            MsgBox("No Existe una Programación a Eliminar", , "Atención")
        End If
    End Sub
End Class