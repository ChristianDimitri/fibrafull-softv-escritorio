<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGeneraCarteras
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR
        Me.Genera_CarteraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Genera_CarteraTableAdapter = New sofTV.DataSetEDGARTableAdapters.Genera_CarteraTableAdapter
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2
        Me.Genera_CarteraDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Genera_CarteraDIGTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.Genera_CarteraDIGTableAdapter
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Genera_CarteraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Genera_CarteraDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(55, 52)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(250, 47)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&Generar Cartera"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(55, 149)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(250, 47)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&CERRAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Genera_CarteraBindingSource
        '
        Me.Genera_CarteraBindingSource.DataMember = "Genera_Cartera"
        Me.Genera_CarteraBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Genera_CarteraTableAdapter
        '
        Me.Genera_CarteraTableAdapter.ClearBeforeFill = True
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Genera_CarteraDIGBindingSource
        '
        Me.Genera_CarteraDIGBindingSource.DataMember = "Genera_CarteraDIG"
        Me.Genera_CarteraDIGBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'Genera_CarteraDIGTableAdapter
        '
        Me.Genera_CarteraDIGTableAdapter.ClearBeforeFill = True
        '
        'FrmGeneraCarteras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(357, 273)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmGeneraCarteras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generar Cartera"
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Genera_CarteraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Genera_CarteraDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents Genera_CarteraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Genera_CarteraTableAdapter As sofTV.DataSetEDGARTableAdapters.Genera_CarteraTableAdapter
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents Genera_CarteraDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Genera_CarteraDIGTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.Genera_CarteraDIGTableAdapter
End Class
