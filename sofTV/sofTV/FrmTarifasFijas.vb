﻿Public Class FrmTarifasFijas
    Private Sub CONTarifasFijas()
        BaseII.limpiaParametros()
        dgvTarifasFijas.DataSource = BaseII.ConsultaDT("CONTarifasFijas")
    End Sub

    Private Sub NUETarifasFijas(ByVal IdTarifa As Integer, ByVal Precio As Decimal)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdTarifa", SqlDbType.Int, IdTarifa)
        BaseII.CreateMyParameter("@Precio", SqlDbType.Decimal, Precio)
        BaseII.Inserta("NUETarifasFijas")
    End Sub

    Private Sub tsbGuardar_Click(sender As System.Object, e As System.EventArgs) Handles tsbGuardar.Click
        dgvTarifasFijas.EndEdit()
        Dim x As Integer
        For x = 0 To dgvTarifasFijas.RowCount - 1

            If dgvTarifasFijas.Item(2, x).Value.ToString.Length = 0 Then
                MessageBox.Show("Precio no válido")
                Exit Sub
            End If

            If IsNumeric(dgvTarifasFijas.Item(2, x).Value) = False Then
                MessageBox.Show("Precio no válido")
                Exit Sub
            End If

            If dgvTarifasFijas.Item(2, x).Value < 0 Then
                MessageBox.Show("Precio no válido")
                Exit Sub
            End If

            NUETarifasFijas(dgvTarifasFijas.Item(0, x).Value, dgvTarifasFijas.Item(2, x).Value)
        Next
        MessageBox.Show("Se guardó con éxito.")
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmTarifasFijas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        CONTarifasFijas()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
End Class