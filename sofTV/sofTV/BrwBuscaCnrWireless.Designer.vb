﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwBuscaCnrWireless
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ConsecutivoLabel = New System.Windows.Forms.Label()
        Me.Numero_de_contratoLabel = New System.Windows.Forms.Label()
        Me.Mac_addresLabel = New System.Windows.Forms.Label()
        Me.PaqueteLabel = New System.Windows.Forms.Label()
        Me.ComandoLabel = New System.Windows.Forms.Label()
        Me.ResultadoLabel = New System.Windows.Forms.Label()
        Me.Descripcion_transaccionLabel = New System.Windows.Forms.Label()
        Me.Clv_OrdenLabel = New System.Windows.Forms.Label()
        Me.Fec_SolLabel = New System.Windows.Forms.Label()
        Me.Fec_EjeLabel = New System.Windows.Forms.Label()
        Me.StatusLabel = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ConsecutivoLabel1 = New System.Windows.Forms.Label()
        Me.Numero_de_contratoLabel1 = New System.Windows.Forms.Label()
        Me.Mac_addresLabel1 = New System.Windows.Forms.Label()
        Me.PaqueteLabel1 = New System.Windows.Forms.Label()
        Me.ComandoLabel1 = New System.Windows.Forms.Label()
        Me.ResultadoLabel1 = New System.Windows.Forms.Label()
        Me.Descripcion_transaccionLabel1 = New System.Windows.Forms.Label()
        Me.Clv_OrdenLabel1 = New System.Windows.Forms.Label()
        Me.Fec_SolLabel1 = New System.Windows.Forms.Label()
        Me.Fec_EjeLabel1 = New System.Windows.Forms.Label()
        Me.StatusLabel1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.BUSCACNRBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BUSCACNRTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCACNRTableAdapter()
        Me.DamePermisosFormBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamePermisosFormTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DamePermisosFormTableAdapter()
        Me.mac_address = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Numerodecontrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Paquete = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comando = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Resultado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripciontransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvOrden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FecSol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FecEje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idFibra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.BUSCACNRBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamePermisosFormBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConsecutivoLabel
        '
        Me.ConsecutivoLabel.AutoSize = True
        Me.ConsecutivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoLabel.Location = New System.Drawing.Point(8, 39)
        Me.ConsecutivoLabel.Name = "ConsecutivoLabel"
        Me.ConsecutivoLabel.Size = New System.Drawing.Size(92, 15)
        Me.ConsecutivoLabel.TabIndex = 5
        Me.ConsecutivoLabel.Text = "Consecutivo :"
        '
        'Numero_de_contratoLabel
        '
        Me.Numero_de_contratoLabel.AutoSize = True
        Me.Numero_de_contratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_de_contratoLabel.Location = New System.Drawing.Point(29, 68)
        Me.Numero_de_contratoLabel.Name = "Numero_de_contratoLabel"
        Me.Numero_de_contratoLabel.Size = New System.Drawing.Size(69, 15)
        Me.Numero_de_contratoLabel.TabIndex = 7
        Me.Numero_de_contratoLabel.Text = "Contrato :"
        '
        'Mac_addresLabel
        '
        Me.Mac_addresLabel.AutoSize = True
        Me.Mac_addresLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mac_addresLabel.Location = New System.Drawing.Point(9, 96)
        Me.Mac_addresLabel.Name = "Mac_addresLabel"
        Me.Mac_addresLabel.Size = New System.Drawing.Size(97, 15)
        Me.Mac_addresLabel.TabIndex = 11
        Me.Mac_addresLabel.Text = "Mac Address :"
        '
        'PaqueteLabel
        '
        Me.PaqueteLabel.AutoSize = True
        Me.PaqueteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaqueteLabel.Location = New System.Drawing.Point(9, 145)
        Me.PaqueteLabel.Name = "PaqueteLabel"
        Me.PaqueteLabel.Size = New System.Drawing.Size(139, 15)
        Me.PaqueteLabel.TabIndex = 13
        Me.PaqueteLabel.Text = "Servicio de Internet :"
        '
        'ComandoLabel
        '
        Me.ComandoLabel.AutoSize = True
        Me.ComandoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComandoLabel.Location = New System.Drawing.Point(15, 196)
        Me.ComandoLabel.Name = "ComandoLabel"
        Me.ComandoLabel.Size = New System.Drawing.Size(76, 15)
        Me.ComandoLabel.TabIndex = 15
        Me.ComandoLabel.Text = "Comando :"
        '
        'ResultadoLabel
        '
        Me.ResultadoLabel.AutoSize = True
        Me.ResultadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ResultadoLabel.Location = New System.Drawing.Point(11, 225)
        Me.ResultadoLabel.Name = "ResultadoLabel"
        Me.ResultadoLabel.Size = New System.Drawing.Size(80, 15)
        Me.ResultadoLabel.TabIndex = 17
        Me.ResultadoLabel.Text = "Resultado :"
        '
        'Descripcion_transaccionLabel
        '
        Me.Descripcion_transaccionLabel.AutoSize = True
        Me.Descripcion_transaccionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion_transaccionLabel.Location = New System.Drawing.Point(14, 254)
        Me.Descripcion_transaccionLabel.Name = "Descripcion_transaccionLabel"
        Me.Descripcion_transaccionLabel.Size = New System.Drawing.Size(91, 15)
        Me.Descripcion_transaccionLabel.TabIndex = 19
        Me.Descripcion_transaccionLabel.Text = "Descripción :"
        '
        'Clv_OrdenLabel
        '
        Me.Clv_OrdenLabel.AutoSize = True
        Me.Clv_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_OrdenLabel.Location = New System.Drawing.Point(5, 381)
        Me.Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Me.Clv_OrdenLabel.Size = New System.Drawing.Size(93, 15)
        Me.Clv_OrdenLabel.TabIndex = 21
        Me.Clv_OrdenLabel.Text = "Clave Orden :"
        '
        'Fec_SolLabel
        '
        Me.Fec_SolLabel.AutoSize = True
        Me.Fec_SolLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_SolLabel.Location = New System.Drawing.Point(14, 410)
        Me.Fec_SolLabel.Name = "Fec_SolLabel"
        Me.Fec_SolLabel.Size = New System.Drawing.Size(114, 15)
        Me.Fec_SolLabel.TabIndex = 23
        Me.Fec_SolLabel.Text = "Fecha Solicitud :"
        '
        'Fec_EjeLabel
        '
        Me.Fec_EjeLabel.AutoSize = True
        Me.Fec_EjeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_EjeLabel.Location = New System.Drawing.Point(7, 439)
        Me.Fec_EjeLabel.Name = "Fec_EjeLabel"
        Me.Fec_EjeLabel.Size = New System.Drawing.Size(121, 15)
        Me.Fec_EjeLabel.TabIndex = 25
        Me.Fec_EjeLabel.Text = "Fecha Ejecución :"
        '
        'StatusLabel
        '
        Me.StatusLabel.AutoSize = True
        Me.StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel.Location = New System.Drawing.Point(73, 467)
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(55, 15)
        Me.StatusLabel.TabIndex = 27
        Me.StatusLabel.Text = "Status :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label10)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(757, 694)
        Me.SplitContainer1.SplitterDistance = 251
        Me.SplitContainer1.TabIndex = 19
        Me.SplitContainer1.TabStop = False
        '
        'ComboBox2
        '
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"P - Orden con Status Pendiente", "E - Orden con Status Ejecutado"})
        Me.ComboBox2.Location = New System.Drawing.Point(17, 471)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(231, 23)
        Me.ComboBox2.TabIndex = 10
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"0 - Pendiente", "1 - Procesada", "2 - Transación Fallida"})
        Me.ComboBox1.Location = New System.Drawing.Point(17, 312)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(231, 24)
        Me.ComboBox1.TabIndex = 6
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(17, 655)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(88, 23)
        Me.Button9.TabIndex = 15
        Me.Button9.Text = "&Buscar"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'TextBox8
        '
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(17, 628)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(118, 22)
        Me.TextBox8.TabIndex = 14
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(14, 610)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(121, 15)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Fecha Ejecución :"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(17, 578)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(88, 23)
        Me.Button8.TabIndex = 13
        Me.Button8.Text = "&Buscar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'TextBox7
        '
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(17, 551)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(118, 22)
        Me.TextBox7.TabIndex = 12
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(14, 533)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(114, 15)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "Fecha Solicitud :"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(17, 498)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(88, 23)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "&Buscar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(14, 453)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 15)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Status :"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(17, 420)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 23)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "&Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(17, 393)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(89, 22)
        Me.TextBox5.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 375)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(93, 15)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Clave Orden :"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(17, 339)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "&Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 294)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 15)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Resultado :"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(17, 260)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(88, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&Buscar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(17, 233)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(219, 22)
        Me.TextBox3.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(14, 215)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 15)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Mac Adress :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(17, 178)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(17, 99)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 1
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(17, 151)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(89, 22)
        Me.TextBox2.TabIndex = 2
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(17, 72)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(88, 22)
        Me.TextBox1.TabIndex = 0
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 12)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(130, 24)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar  Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Consecutivo :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Contrato :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.mac_address, Me.Consecutivo, Me.Numerodecontrato, Me.Paquete, Me.Comando, Me.Resultado, Me.Descripciontransaccion, Me.ClvOrden, Me.FecSol, Me.FecEje, Me.Status, Me.idFibra})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(502, 694)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Chocolate
        Me.Panel1.Controls.Add(Me.ConsecutivoLabel)
        Me.Panel1.Controls.Add(Me.ConsecutivoLabel1)
        Me.Panel1.Controls.Add(Me.Numero_de_contratoLabel)
        Me.Panel1.Controls.Add(Me.Numero_de_contratoLabel1)
        Me.Panel1.Controls.Add(Me.Mac_addresLabel)
        Me.Panel1.Controls.Add(Me.Mac_addresLabel1)
        Me.Panel1.Controls.Add(Me.PaqueteLabel)
        Me.Panel1.Controls.Add(Me.PaqueteLabel1)
        Me.Panel1.Controls.Add(Me.ComandoLabel)
        Me.Panel1.Controls.Add(Me.ComandoLabel1)
        Me.Panel1.Controls.Add(Me.ResultadoLabel)
        Me.Panel1.Controls.Add(Me.ResultadoLabel1)
        Me.Panel1.Controls.Add(Me.Descripcion_transaccionLabel)
        Me.Panel1.Controls.Add(Me.Descripcion_transaccionLabel1)
        Me.Panel1.Controls.Add(Me.Clv_OrdenLabel)
        Me.Panel1.Controls.Add(Me.Clv_OrdenLabel1)
        Me.Panel1.Controls.Add(Me.Fec_SolLabel)
        Me.Panel1.Controls.Add(Me.Fec_SolLabel1)
        Me.Panel1.Controls.Add(Me.Fec_EjeLabel)
        Me.Panel1.Controls.Add(Me.Fec_EjeLabel1)
        Me.Panel1.Controls.Add(Me.StatusLabel)
        Me.Panel1.Controls.Add(Me.StatusLabel1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(762, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(252, 521)
        Me.Panel1.TabIndex = 8
        '
        'ConsecutivoLabel1
        '
        Me.ConsecutivoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.ConsecutivoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoLabel1.Location = New System.Drawing.Point(102, 35)
        Me.ConsecutivoLabel1.Name = "ConsecutivoLabel1"
        Me.ConsecutivoLabel1.Size = New System.Drawing.Size(100, 23)
        Me.ConsecutivoLabel1.TabIndex = 6
        '
        'Numero_de_contratoLabel1
        '
        Me.Numero_de_contratoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Numero_de_contratoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_de_contratoLabel1.Location = New System.Drawing.Point(102, 64)
        Me.Numero_de_contratoLabel1.Name = "Numero_de_contratoLabel1"
        Me.Numero_de_contratoLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Numero_de_contratoLabel1.TabIndex = 8
        '
        'Mac_addresLabel1
        '
        Me.Mac_addresLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Mac_addresLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mac_addresLabel1.Location = New System.Drawing.Point(9, 117)
        Me.Mac_addresLabel1.Name = "Mac_addresLabel1"
        Me.Mac_addresLabel1.Size = New System.Drawing.Size(223, 23)
        Me.Mac_addresLabel1.TabIndex = 12
        '
        'PaqueteLabel1
        '
        Me.PaqueteLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.PaqueteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaqueteLabel1.Location = New System.Drawing.Point(11, 164)
        Me.PaqueteLabel1.Name = "PaqueteLabel1"
        Me.PaqueteLabel1.Size = New System.Drawing.Size(221, 23)
        Me.PaqueteLabel1.TabIndex = 14
        '
        'ComandoLabel1
        '
        Me.ComandoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.ComandoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComandoLabel1.Location = New System.Drawing.Point(96, 193)
        Me.ComandoLabel1.Name = "ComandoLabel1"
        Me.ComandoLabel1.Size = New System.Drawing.Size(136, 23)
        Me.ComandoLabel1.TabIndex = 16
        '
        'ResultadoLabel1
        '
        Me.ResultadoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.ResultadoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ResultadoLabel1.Location = New System.Drawing.Point(96, 222)
        Me.ResultadoLabel1.Name = "ResultadoLabel1"
        Me.ResultadoLabel1.Size = New System.Drawing.Size(29, 23)
        Me.ResultadoLabel1.TabIndex = 18
        '
        'Descripcion_transaccionLabel1
        '
        Me.Descripcion_transaccionLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Descripcion_transaccionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion_transaccionLabel1.Location = New System.Drawing.Point(18, 275)
        Me.Descripcion_transaccionLabel1.Name = "Descripcion_transaccionLabel1"
        Me.Descripcion_transaccionLabel1.Size = New System.Drawing.Size(214, 95)
        Me.Descripcion_transaccionLabel1.TabIndex = 20
        '
        'Clv_OrdenLabel1
        '
        Me.Clv_OrdenLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Clv_OrdenLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_OrdenLabel1.Location = New System.Drawing.Point(102, 377)
        Me.Clv_OrdenLabel1.Name = "Clv_OrdenLabel1"
        Me.Clv_OrdenLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Clv_OrdenLabel1.TabIndex = 22
        '
        'Fec_SolLabel1
        '
        Me.Fec_SolLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Fec_SolLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_SolLabel1.Location = New System.Drawing.Point(133, 406)
        Me.Fec_SolLabel1.Name = "Fec_SolLabel1"
        Me.Fec_SolLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Fec_SolLabel1.TabIndex = 24
        '
        'Fec_EjeLabel1
        '
        Me.Fec_EjeLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Fec_EjeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_EjeLabel1.Location = New System.Drawing.Point(133, 435)
        Me.Fec_EjeLabel1.Name = "Fec_EjeLabel1"
        Me.Fec_EjeLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Fec_EjeLabel1.TabIndex = 26
        '
        'StatusLabel1
        '
        Me.StatusLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel1.Location = New System.Drawing.Point(133, 464)
        Me.StatusLabel1.Name = "StatusLabel1"
        Me.StatusLabel1.Size = New System.Drawing.Size(100, 23)
        Me.StatusLabel1.TabIndex = 28
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(154, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Servicio"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(838, 670)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 16
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'BUSCACNRBindingSource
        '
        Me.BUSCACNRBindingSource.DataMember = "BUSCACNR"
        Me.BUSCACNRBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.EnforceConstraints = False
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BUSCACNRTableAdapter
        '
        Me.BUSCACNRTableAdapter.ClearBeforeFill = True
        '
        'DamePermisosFormBindingSource
        '
        Me.DamePermisosFormBindingSource.DataMember = "DamePermisosForm"
        Me.DamePermisosFormBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DamePermisosFormTableAdapter
        '
        Me.DamePermisosFormTableAdapter.ClearBeforeFill = True
        '
        'mac_address
        '
        Me.mac_address.DataPropertyName = "mac_address"
        Me.mac_address.HeaderText = "Macaddress"
        Me.mac_address.Name = "mac_address"
        Me.mac_address.ReadOnly = True
        '
        'Consecutivo
        '
        Me.Consecutivo.DataPropertyName = "consecutivo"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consecutivo.DefaultCellStyle = DataGridViewCellStyle2
        Me.Consecutivo.HeaderText = "Consecutivo"
        Me.Consecutivo.Name = "Consecutivo"
        Me.Consecutivo.ReadOnly = True
        '
        'Numerodecontrato
        '
        Me.Numerodecontrato.DataPropertyName = "numero_de_contrato"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numerodecontrato.DefaultCellStyle = DataGridViewCellStyle3
        Me.Numerodecontrato.HeaderText = "Contrato"
        Me.Numerodecontrato.Name = "Numerodecontrato"
        Me.Numerodecontrato.ReadOnly = True
        '
        'Paquete
        '
        Me.Paquete.DataPropertyName = "paquete"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paquete.DefaultCellStyle = DataGridViewCellStyle4
        Me.Paquete.HeaderText = "Servicio de Internet"
        Me.Paquete.Name = "Paquete"
        Me.Paquete.ReadOnly = True
        Me.Paquete.Width = 200
        '
        'Comando
        '
        Me.Comando.DataPropertyName = "comando"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Comando.DefaultCellStyle = DataGridViewCellStyle5
        Me.Comando.HeaderText = "Comando"
        Me.Comando.Name = "Comando"
        Me.Comando.ReadOnly = True
        '
        'Resultado
        '
        Me.Resultado.DataPropertyName = "resultado"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Resultado.DefaultCellStyle = DataGridViewCellStyle6
        Me.Resultado.HeaderText = "Resultado"
        Me.Resultado.Name = "Resultado"
        Me.Resultado.ReadOnly = True
        '
        'Descripciontransaccion
        '
        Me.Descripciontransaccion.DataPropertyName = "descripcion_transaccion"
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripciontransaccion.DefaultCellStyle = DataGridViewCellStyle7
        Me.Descripciontransaccion.HeaderText = "Descripción"
        Me.Descripciontransaccion.Name = "Descripciontransaccion"
        Me.Descripciontransaccion.ReadOnly = True
        Me.Descripciontransaccion.Width = 200
        '
        'ClvOrden
        '
        Me.ClvOrden.DataPropertyName = "Clv_Orden"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClvOrden.DefaultCellStyle = DataGridViewCellStyle8
        Me.ClvOrden.HeaderText = "Clave Orden"
        Me.ClvOrden.Name = "ClvOrden"
        Me.ClvOrden.ReadOnly = True
        '
        'FecSol
        '
        Me.FecSol.DataPropertyName = "Fec_Sol"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FecSol.DefaultCellStyle = DataGridViewCellStyle9
        Me.FecSol.HeaderText = "Fecha Solicitud"
        Me.FecSol.Name = "FecSol"
        Me.FecSol.ReadOnly = True
        Me.FecSol.Width = 150
        '
        'FecEje
        '
        Me.FecEje.DataPropertyName = "Fec_Eje"
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FecEje.DefaultCellStyle = DataGridViewCellStyle10
        Me.FecEje.HeaderText = "Fecha Ejecución"
        Me.FecEje.Name = "FecEje"
        Me.FecEje.ReadOnly = True
        Me.FecEje.Width = 150
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Status.DefaultCellStyle = DataGridViewCellStyle11
        Me.Status.HeaderText = "Status Orden"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'idFibra
        '
        Me.idFibra.DataPropertyName = "clave_unica_net"
        Me.idFibra.HeaderText = "clave_unica_net"
        Me.idFibra.Name = "idFibra"
        Me.idFibra.ReadOnly = True
        Me.idFibra.Visible = False
        '
        'BrwBuscaCnrWireless
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1016, 750)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "BrwBuscaCnrWireless"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Interfaz Wireless"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BUSCACNRBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamePermisosFormBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BUSCACNRBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents BUSCACNRTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCACNRTableAdapter
    Friend WithEvents ConsecutivoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Numero_de_contratoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Mac_addresLabel1 As System.Windows.Forms.Label
    Friend WithEvents PaqueteLabel1 As System.Windows.Forms.Label
    Friend WithEvents ComandoLabel1 As System.Windows.Forms.Label
    Friend WithEvents ResultadoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Descripcion_transaccionLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_OrdenLabel1 As System.Windows.Forms.Label
    Friend WithEvents Fec_SolLabel1 As System.Windows.Forms.Label
    Friend WithEvents Fec_EjeLabel1 As System.Windows.Forms.Label
    Friend WithEvents StatusLabel1 As System.Windows.Forms.Label
    Friend WithEvents MacaddresDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DamePermisosFormBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamePermisosFormTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DamePermisosFormTableAdapter
    Friend WithEvents ConsecutivoLabel As System.Windows.Forms.Label
    Friend WithEvents Numero_de_contratoLabel As System.Windows.Forms.Label
    Friend WithEvents Mac_addresLabel As System.Windows.Forms.Label
    Friend WithEvents PaqueteLabel As System.Windows.Forms.Label
    Friend WithEvents ComandoLabel As System.Windows.Forms.Label
    Friend WithEvents ResultadoLabel As System.Windows.Forms.Label
    Friend WithEvents Descripcion_transaccionLabel As System.Windows.Forms.Label
    Friend WithEvents Clv_OrdenLabel As System.Windows.Forms.Label
    Friend WithEvents Fec_SolLabel As System.Windows.Forms.Label
    Friend WithEvents Fec_EjeLabel As System.Windows.Forms.Label
    Friend WithEvents StatusLabel As System.Windows.Forms.Label
    Friend WithEvents mac_address As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Consecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numerodecontrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Paquete As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Comando As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Resultado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripciontransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvOrden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FecSol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FecEje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idFibra As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
