﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambioWifiAFtthPorContrato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CONTRATOLabel = New System.Windows.Forms.Label()
        Me.NOMBRELabel = New System.Windows.Forms.Label()
        Me.CALLELabel = New System.Windows.Forms.Label()
        Me.COLONIALabel = New System.Windows.Forms.Label()
        Me.NUMEROLabel = New System.Windows.Forms.Label()
        Me.CIUDADLabel = New System.Windows.Forms.Label()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ResetServCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ResetServCteTableAdapter = New sofTV.DataSetEricTableAdapters.ResetServCteTableAdapter()
        Me.DameClientesActivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClientesActivosTableAdapter = New sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.CALLETextBox = New System.Windows.Forms.TextBox()
        Me.COLONIATextBox = New System.Windows.Forms.TextBox()
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.CIUDADTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ButtonAceptar = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TextBoxNetActual = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBoxNetNuevo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResetServCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        Me.CONTRATOLabel.AutoSize = True
        Me.CONTRATOLabel.Location = New System.Drawing.Point(14, 44)
        Me.CONTRATOLabel.Name = "CONTRATOLabel"
        Me.CONTRATOLabel.Size = New System.Drawing.Size(74, 16)
        Me.CONTRATOLabel.TabIndex = 3
        Me.CONTRATOLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        Me.NOMBRELabel.AutoSize = True
        Me.NOMBRELabel.Location = New System.Drawing.Point(14, 70)
        Me.NOMBRELabel.Name = "NOMBRELabel"
        Me.NOMBRELabel.Size = New System.Drawing.Size(71, 16)
        Me.NOMBRELabel.TabIndex = 5
        Me.NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        Me.CALLELabel.AutoSize = True
        Me.CALLELabel.Location = New System.Drawing.Point(14, 96)
        Me.CALLELabel.Name = "CALLELabel"
        Me.CALLELabel.Size = New System.Drawing.Size(52, 16)
        Me.CALLELabel.TabIndex = 7
        Me.CALLELabel.Text = "Calle :"
        '
        'COLONIALabel
        '
        Me.COLONIALabel.AutoSize = True
        Me.COLONIALabel.Location = New System.Drawing.Point(14, 122)
        Me.COLONIALabel.Name = "COLONIALabel"
        Me.COLONIALabel.Size = New System.Drawing.Size(69, 16)
        Me.COLONIALabel.TabIndex = 9
        Me.COLONIALabel.Text = "Colonia :"
        '
        'NUMEROLabel
        '
        Me.NUMEROLabel.AutoSize = True
        Me.NUMEROLabel.Location = New System.Drawing.Point(384, 125)
        Me.NUMEROLabel.Name = "NUMEROLabel"
        Me.NUMEROLabel.Size = New System.Drawing.Size(24, 16)
        Me.NUMEROLabel.TabIndex = 11
        Me.NUMEROLabel.Text = "# :"
        '
        'CIUDADLabel
        '
        Me.CIUDADLabel.AutoSize = True
        Me.CIUDADLabel.Location = New System.Drawing.Point(14, 150)
        Me.CIUDADLabel.Name = "CIUDADLabel"
        Me.CIUDADLabel.Size = New System.Drawing.Size(65, 16)
        Me.CIUDADLabel.TabIndex = 13
        Me.CIUDADLabel.Text = "Ciudad :"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.EnforceConstraints = False
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ResetServCteBindingSource
        '
        Me.ResetServCteBindingSource.DataMember = "ResetServCte"
        Me.ResetServCteBindingSource.DataSource = Me.DataSetEric
        '
        'ResetServCteTableAdapter
        '
        Me.ResetServCteTableAdapter.ClearBeforeFill = True
        '
        'DameClientesActivosBindingSource
        '
        Me.DameClientesActivosBindingSource.DataMember = "DameClientesActivos"
        Me.DameClientesActivosBindingSource.DataSource = Me.DataSetEric
        '
        'DameClientesActivosTableAdapter
        '
        Me.DameClientesActivosTableAdapter.ClearBeforeFill = True
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(114, 67)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.ReadOnly = True
        Me.NOMBRETextBox.Size = New System.Drawing.Size(427, 22)
        Me.NOMBRETextBox.TabIndex = 6
        Me.NOMBRETextBox.TabStop = False
        '
        'CALLETextBox
        '
        Me.CALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.CALLETextBox.Location = New System.Drawing.Point(114, 93)
        Me.CALLETextBox.Name = "CALLETextBox"
        Me.CALLETextBox.ReadOnly = True
        Me.CALLETextBox.Size = New System.Drawing.Size(257, 22)
        Me.CALLETextBox.TabIndex = 8
        Me.CALLETextBox.TabStop = False
        '
        'COLONIATextBox
        '
        Me.COLONIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "COLONIA", True))
        Me.COLONIATextBox.Location = New System.Drawing.Point(114, 119)
        Me.COLONIATextBox.Name = "COLONIATextBox"
        Me.COLONIATextBox.ReadOnly = True
        Me.COLONIATextBox.Size = New System.Drawing.Size(257, 22)
        Me.COLONIATextBox.TabIndex = 10
        Me.COLONIATextBox.TabStop = False
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Location = New System.Drawing.Point(414, 122)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.ReadOnly = True
        Me.NUMEROTextBox.Size = New System.Drawing.Size(104, 22)
        Me.NUMEROTextBox.TabIndex = 12
        Me.NUMEROTextBox.TabStop = False
        '
        'CIUDADTextBox
        '
        Me.CIUDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CIUDAD", True))
        Me.CIUDADTextBox.Location = New System.Drawing.Point(114, 147)
        Me.CIUDADTextBox.Name = "CIUDADTextBox"
        Me.CIUDADTextBox.ReadOnly = True
        Me.CIUDADTextBox.Size = New System.Drawing.Size(104, 22)
        Me.CIUDADTextBox.TabIndex = 14
        Me.CIUDADTextBox.TabStop = False
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(229, 38)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(44, 23)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(114, 38)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 22)
        Me.TextBox1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.NOMBRETextBox)
        Me.GroupBox1.Controls.Add(Me.CIUDADTextBox)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel)
        Me.GroupBox1.Controls.Add(Me.NUMEROTextBox)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.COLONIATextBox)
        Me.GroupBox1.Controls.Add(Me.CONTRATOLabel)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel)
        Me.GroupBox1.Controls.Add(Me.CALLETextBox)
        Me.GroupBox1.Controls.Add(Me.CALLELabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(38, 21)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(582, 193)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Enabled = False
        Me.ButtonAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAceptar.Location = New System.Drawing.Point(248, 359)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(136, 36)
        Me.ButtonAceptar.TabIndex = 2
        Me.ButtonAceptar.Text = "Aceptar"
        Me.ButtonAceptar.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(484, 359)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBoxNetActual)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(38, 234)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(270, 86)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Servicios Actuales"
        '
        'TextBoxNetActual
        '
        Me.TextBoxNetActual.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.TextBoxNetActual.Location = New System.Drawing.Point(12, 49)
        Me.TextBoxNetActual.Name = "TextBoxNetActual"
        Me.TextBoxNetActual.ReadOnly = True
        Me.TextBoxNetActual.Size = New System.Drawing.Size(238, 22)
        Me.TextBoxNetActual.TabIndex = 66
        Me.TextBoxNetActual.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(11, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 15)
        Me.Label1.TabIndex = 62
        Me.Label1.Text = "Internet:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBoxNetNuevo)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(350, 234)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(270, 86)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Servicios Equivalentes"
        '
        'TextBoxNetNuevo
        '
        Me.TextBoxNetNuevo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.TextBoxNetNuevo.Location = New System.Drawing.Point(12, 49)
        Me.TextBoxNetNuevo.Name = "TextBoxNetNuevo"
        Me.TextBoxNetNuevo.ReadOnly = True
        Me.TextBoxNetNuevo.Size = New System.Drawing.Size(238, 22)
        Me.TextBoxNetNuevo.TabIndex = 66
        Me.TextBoxNetNuevo.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(11, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 15)
        Me.Label4.TabIndex = 62
        Me.Label4.Text = "Internet:"
        '
        'FrmCambioWifiAFtthPorContrato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(661, 412)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmCambioWifiAFtthPorContrato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Resetear Aparato"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResetServCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ResetServCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ResetServCteTableAdapter As sofTV.DataSetEricTableAdapters.ResetServCteTableAdapter
    Friend WithEvents DameClientesActivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClientesActivosTableAdapter As sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CONTRATOLabel As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel As System.Windows.Forms.Label
    Friend WithEvents CALLELabel As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxNetActual As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxNetNuevo As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class

