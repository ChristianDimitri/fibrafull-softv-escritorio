Imports System.Data.SqlClient
Public Class FrmSelPromocion_Rep

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim x As Integer = 0
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        If x > 0 Then
            For y = 0 To (x - 1)
                Me.ListBox2.SelectedIndex = y
                If (Me.ListBox1.Text = Me.ListBox2.Text) Then
                    MsgBox("La Colonia ya esta en la lista", MsgBoxStyle.Information)
                    Exit Sub
                    'Else
                    '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
                    '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
                    '    Exit Sub
                End If
            Next
            Me.ListBox2.Items.Add(Me.ListBox1.Text)
            Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        Else
            Me.ListBox2.Items.Add(Me.ListBox1.Text)
            Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim x As Integer
        Dim y As Integer
        x = Me.ListBox1.Items.Count()
        If Me.ListBox2.Items.Count() > 0 Then
            MsgBox("Primero Borre la(s) Promoción(es) Seleccionada(s)", MsgBoxStyle.Information)
        Else
            Me.ListBox1.SelectedIndex = 0
            For y = 1 To x
                Me.ListBox1.SelectedIndex = (y - 1)
                Me.ListBox2.Items.Add(Me.ListBox1.Text)
                Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
            Next
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (Me.ListBox2.SelectedIndex <> -1) Then
            Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
            Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
            Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        Else
            MsgBox("Selecciona primero un valor a borrar")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim x As Integer
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        For y = 0 To (x - 1)
            Me.ListBox2.Items.RemoveAt(0)
            Me.ListBox3.Items.RemoveAt(0)
        Next
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim x As Integer = 0
        Dim y As Integer
        x = Me.ListBox3.Items.Count()
        For y = 0 To (x - 1)
            Me.ListBox3.SelectedIndex = y
            CON.Open()
            Me.Inserta_Sel_PromocionTableAdapter.Connection = CON
            Me.Inserta_Sel_PromocionTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Sel_Promocion, LocClv_session, CInt(Me.ListBox3.Text))
            CON.Close()
        Next


        If (Me.ListBox2.Items.Count()) > 1 Then
            LocDescr4 = "Varias Promociones"
            My.Forms.FrmSelFechas.Show()
            'My.Forms.FrmSelEstado.Show()
        Else
            Me.ListBox2.SelectedIndex = 0
            LocDescr4 = Me.ListBox2.Text
            My.Forms.FrmSelFechas.Show()

        End If

        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelPromocion_Rep_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        Dim x As Integer = 0
        CON.Open()
        Me.Muestra_PromocionTableAdapter.Connection = CON
        Me.Muestra_PromocionTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Promocion, LocClv_session, GloClv_tipser2)
        CON.Close()
        x = Me.ListBox1.Items.Count()
        If x = 0 Then
            MsgBox("No Hay Promociones Asignadas Promocion", MsgBoxStyle.Information)
            Me.Close()
        End If
        colorea(Me, Me.Name)
    End Sub
End Class