﻿Imports System.Data.SqlClient
Public Class FrmCambioWifiAFtthPorContrato

    Dim Clv_TipSer As Integer
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Private TipSer As String = Nothing
    Private Mac As String = Nothing
    Private yaesta As Integer = 0

    Private Sub GuardaDatosBitacora()
        Try
            

            bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Text, "Cambio de Wifi a Ftth", "", "", LocClv_Ciudad)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub FrmReset_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            Me.TextBox1.Text = eContrato
            CON.Open()
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            CON.Close()

            If yaesta = 0 Then
                BuscarCliente()
            End If
            yaesta = 0

        End If

    End Sub

    Private Sub FrmReset_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
       
        ButtonAceptar.Enabled = False
        eContrato = 0

        Clv_TipSer = 2
    End Sub
    Private Sub BuscarCliente()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameClientesActivosTableAdapter.Connection = CON
        Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        CON.Close()
        BuscarServ()
    End Sub
    Private Sub BuscarServ()

        eRes = 0
        eMsg = ""

        Dim CONE As New SqlConnection(MiConexion)
        CONE.Open()
        Dim Comando = New SqlClient.SqlCommand
        With Comando
            .Connection = CONE
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            .CommandText = "MuestraServInternetCambioWifiAFtth"

            '' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Contrato", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@Res", SqlDbType.VarChar, 50)
            Dim prm3 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
            Dim prm4 As New SqlParameter("@servicioWIreless", SqlDbType.VarChar, 150)
            Dim prm5 As New SqlParameter("@servicioFtth", SqlDbType.VarChar, 150)

            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Output
            prm3.Direction = ParameterDirection.Output
            prm4.Direction = ParameterDirection.Output
            prm5.Direction = ParameterDirection.Output


            prm.Value = Me.TextBox1.Text
            prm2.Value = eRes
            prm3.Value = eMsg
            prm4.Value = TextBoxNetActual.Text
            prm5.Value = TextBoxNetNuevo.Text

            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            .Parameters.Add(prm5)

            Dim i As Integer = Comando.ExecuteNonQuery()
            eRes = prm2.Value
            eMsg = prm3.Value

            TextBoxNetActual.Text = prm4.Value
            TextBoxNetNuevo.Text = prm5.Value
        End With
        CONE.Close()

        If eRes = 1 Then
            yaesta = 1
            MsgBox(eMsg)
            ButtonAceptar.Enabled = False
        Else
            ButtonAceptar.Enabled = True
        End If


    End Sub





    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Me.TextBox1.Text)

        BaseII.ProcedimientoOutPut("GENERARCambioWifiAFtthPorContrato")

        GuardaDatosBitacora()
        MsgBox("El cambio fue exitoso")
        Me.Close()

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                BuscarCliente()
            End If
        End If
    End Sub


    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    
 



End Class