Imports System.Data.SqlClient
Public Class FrmCodigosMexico

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmCodigosMexico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim conload As New SqlConnection(MiConexion)
        If opcion = "N" Then
            Me.Consulta_CodigosMexicoBindingSource.AddNew()
        ElseIf opcion = "M" Or opcion = "C" Then
            conload.Open()
            Me.Consulta_CodigosMexicoTableAdapter.Connection = conload
            Me.Consulta_CodigosMexicoTableAdapter.Fill(Me.DataSetLidia2.consulta_CodigosMexico, clv_codigomex, "", "", 2)
            conload.Close()
        End If
        If opcion = "C" Then

            Me.Panel1.Enabled = False

        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

    End Sub


    Private Sub RegionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RegionTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.RegionTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub


    Private Sub ASLTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ASLTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ASLTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub SerieTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SerieTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.SerieTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub NumeracionInicialTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NumeracionInicialTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NumeracionInicialTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub NumeracionFinalTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NumeracionFinalTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NumeracionFinalTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub InicialTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles InicialTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.InicialTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub FinalTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FinalTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.FinalTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub OcupacionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles OcupacionTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.OcupacionTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub IdOperadorTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles IdOperadorTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.IdOperadorTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim conDel As New SqlConnection(MiConexion)
        conDel.Open()
        Me.Consulta_CodigosMexicoTableAdapter.Connection = conDel
        Me.Consulta_CodigosMexicoTableAdapter.Delete(Me.Clv_CodigoTextBox.Text)
        conDel.Close()
        bec_bnd = True
        MsgBox("Se ha Eliminado con �xito", MsgBoxStyle.Information)
        Me.Close()
    End Sub

    Private Sub Consulta_CodigosMexicoBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_CodigosMexicoBindingNavigatorSaveItem.Click
        Dim conSave As New SqlConnection(MiConexion)
        Me.Validate()
        Me.Consulta_CodigosMexicoBindingSource.EndEdit()
        conSave.Open()
        Me.Consulta_CodigosMexicoTableAdapter.Connection = conSave
        Me.Consulta_CodigosMexicoTableAdapter.Update(Me.DataSetLidia2.consulta_CodigosMexico)
        conSave.Close()
        bec_bnd = True
        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
        Me.Close()
    End Sub
End Class