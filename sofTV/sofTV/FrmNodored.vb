﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Public Class FrmNodored

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If cmbNodoRED.SelectedValue = 0 Then
            MsgBox("Debe de seleccionar un nodo de red.", MsgBoxStyle.Information)
        ElseIf cmbNodoRED.SelectedValue > 0 Then

            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand("uspGuardaNodoRedCliente", conexion)
            comando.CommandType = CommandType.StoredProcedure

            Dim parametro As New SqlParameter("@contrato", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = FrmOrdSer.ContratoTextBox.Text
            comando.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@NodoRed", SqlDbType.BigInt)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = cmbNodoRED.SelectedValue
            comando.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@op", SqlDbType.BigInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = 0
            comando.Parameters.Add(parametro2)


            Try
                conexion.Open()
                comando.ExecuteNonQuery()
                conexion.Close()
                conexion.Dispose()
                NODO = 1

            Catch ex As Exception
                conexion.Close()
                conexion.Dispose()
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
            Me.Close()
        End If
    End Sub
    Private Sub FrmNodored_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        WIRELES1 = 3
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        WIRELES1 = 3
        Me.Close()
    End Sub

    Private Sub FrmNodored_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        NODO = 0
        ConsultaNodoRed()

    End Sub
    Private Sub ConsultaNodoRed()
        Dim CON As New SqlConnection(MiConexion)
        Dim STR As New StringBuilder

        STR.Append("EXEC uspNodoDeRed ")
        STR.Append(CStr(0))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(STR.ToString, CON)

        Try
            CON.Open()
            DA.Fill(DT)
            Me.cmbNodoRED.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


End Class