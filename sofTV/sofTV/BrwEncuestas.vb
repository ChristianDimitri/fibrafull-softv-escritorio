Imports System.Data.SqlClient
Imports System.Text
Public Class BrwEncuestas

    Private Sub MuestraEncuestas(ByVal Op As Integer, ByVal Nombre As String, ByVal Descripcion As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraEncuestas ")
        strSQL.Append(CStr(Op) & ", ")
        strSQL.Append("'" & Nombre & "', ")
        strSQL.Append("'" & Descripcion & "'")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridViewEncuestas.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub


    Private Sub TextBoxNombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxNombre.KeyPress
        If Asc(e.KeyChar) <> 13 And TextBoxNombre.Text.Length <> 0 Then
            MuestraEncuestas(0, String.Empty, String.Empty)
            Exit Sub
        End If
        MuestraEncuestas(1, TextBoxNombre.Text, String.Empty)

    End Sub

    Private Sub ButtonBusNombre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBusNombre.Click
        If TextBoxNombre.Text.Length = 0 Then
            MuestraEncuestas(0, String.Empty, String.Empty)
            Exit Sub
        End If
        MuestraEncuestas(1, TextBoxNombre.Text, String.Empty)
    End Sub

    Private Sub TextBoxDescripcion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxDescripcion.KeyPress
        If Asc(e.KeyChar) <> 13 And TextBoxDescripcion.Text.Length <> 0 Then
            MuestraEncuestas(0, String.Empty, String.Empty)
            Exit Sub
        End If
        MuestraEncuestas(2, String.Empty, TextBoxDescripcion.Text)
    End Sub

    Private Sub ButtonBusDescripcion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBusDescripcion.Click
        If TextBoxDescripcion.Text.Length = 0 Then
            MuestraEncuestas(0, String.Empty, String.Empty)
            Exit Sub
        End If
        MuestraEncuestas(2, String.Empty, TextBoxDescripcion.Text)
    End Sub

    Private Sub ButtonNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNuevo.Click
        eOpcion = "N"
        eIDEncuesta = 0
        eBnd = False
        FrmEncuestas.Show()
    End Sub

    Private Sub ButtonConsultar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonConsultar.Click
        eOpcion = "C"
        eIDEncuesta = DataGridViewEncuestas.SelectedCells.Item(0).Value
        eBnd = False
        FrmEncuestas.Show()
    End Sub

    Private Sub ButtonModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonModificar.Click
        eOpcion = "M"
        eIDEncuesta = DataGridViewEncuestas.SelectedCells.Item(0).Value
        eBnd = False
        FrmEncuestas.Show()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub BrwEncuestas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBnd = True Then
            eBnd = False
            MuestraEncuestas(0, String.Empty, String.Empty)
        End If
    End Sub

    Private Sub BrwEncuestas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraEncuestas(0, String.Empty, String.Empty)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
End Class