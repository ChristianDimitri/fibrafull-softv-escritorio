Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic

Public Class FrmImprimirComision

    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Delegate Sub Reporte()
    Delegate Sub Forms(ByVal Mycontrol As FrmImprimirComision, ByVal Text As String)

    Private Sub DelegadoForm(ByVal MyControl As FrmImprimirComision, ByVal text As String)
        MyControl.Text = text
    End Sub



    Private Sub ConfigureCrystalReports()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            eTituloComision = ""


            If eOpVentas = 1 Then

                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If
                reportPath = RutaReportes + "\ReportComisiones.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(0, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(1, eFechaFin)
                ''@Op
                'customersByCityReport.SetParameterValue(2, eOp)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(3, eClv_Session)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, eOp)
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))

                Dim listatablas As New List(Of String)
                listatablas.Add("ComisionesVendedores")
                DS = BaseII.ConsultaDS("ComisionesVendedores", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 2 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de N�mero de Ventas"
                End If


                reportPath = RutaReportes + "\ReportNumeroVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)
                '@Clv_Session
                customersByCityReport.SetParameterValue(3, eClv_Session)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(4, eTipSer)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Servicio").Text = "'" & eServicio & "'"



            ElseIf eOpVentas = 3 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If



                reportPath = RutaReportes + "\ReportStatusVentas.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(0, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(1, eFechaFin)
                ''@Op
                'customersByCityReport.SetParameterValue(2, eOp)
                ''@ServicioPadre
                'customersByCityReport.SetParameterValue(3, eTipSer)
                ''@Contratado
                'customersByCityReport.SetParameterValue(4, eCont)
                ''@Instalado
                'customersByCityReport.SetParameterValue(5, eInst)
                ''@Desconectado
                'customersByCityReport.SetParameterValue(6, eDesc)
                ''@Suspendido
                'customersByCityReport.SetParameterValue(7, eSusp)
                ''@Baja
                'customersByCityReport.SetParameterValue(8, eBaja)
                ''@Fuera de Area
                'customersByCityReport.SetParameterValue(9, eFuera)
                ''@Clv_SessionVendedores
                'customersByCityReport.SetParameterValue(10, eClv_Session)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, CInt(eOp))
                BaseII.CreateMyParameter("@ServicioPadre", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Cont", SqlDbType.Int, CInt(eCont))
                BaseII.CreateMyParameter("@Inst", SqlDbType.Int, CInt(eInst))
                BaseII.CreateMyParameter("@Desc", SqlDbType.Int, CInt(eDesc))
                BaseII.CreateMyParameter("@Susp", SqlDbType.Int, CInt(eSusp))
                BaseII.CreateMyParameter("@Baja", SqlDbType.Int, CInt(eBaja))
                BaseII.CreateMyParameter("@Fuera", SqlDbType.Int, CInt(eFuera))
                BaseII.CreateMyParameter("@Clv_SessionVendedores", SqlDbType.BigInt, CInt(eClv_Session))

                Dim listatablas As New List(Of String)
                listatablas.Add("StatusDeVentas")
                DS = BaseII.ConsultaDS("StatusDeVentas", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Servicio").Text = "'" & eServicio & "'"
                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"


            ElseIf eOpVentas = 4 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Folios Faltantes"
                End If



                reportPath = RutaReportes + "\ReportFolios.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                ''@FechaInicial
                'customersByCityReport.SetParameterValue(0, eFechaIni)
                ''@FechaFinal
                'customersByCityReport.SetParameterValue(1, eFechaFin)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))

                Dim listatablas As New List(Of String)
                listatablas.Add("FolioDeVentasVendedor")
                DS = BaseII.ConsultaDS("FolioDeVentasVendedor", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"




            ElseIf eOpVentas = 11 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Consolidado de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Consolidado de Ventas de Servicio " & eServicio
                Else
                    eServicio = "Gr�fica Consolidado de Ventas"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_1.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(1, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(2, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("GraficaVentas_1")
                DS = BaseII.ConsultaDS("GraficaVentas_1", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 12 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If


                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales de Servicio  " & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales de Varios Tipos de Servicio"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_2.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(3, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("GraficaVentas_2")
                listatablas.Add("SUCURSALES")
                DS = BaseII.ConsultaDS("GraficaVentas_2", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 13 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales Por Servicios de " & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales Por Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_3.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(3, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("GraficaVentas_3")
                listatablas.Add("Servicios")
                DS = BaseII.ConsultaDS("GraficaVentas_3", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 14 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Del Depto. de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Servicio de " & eServicio
                Else
                    If eBndOpVentas = True Then
                        eBndOpVentas = False
                        eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Varios Tipos de Servicio"
                    Else
                        eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Varios Servicios"
                    End If
                End If
                reportPath = RutaReportes + "\ReporteGrafica_4.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(3, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("GraficaVentas_4")
                listatablas.Add("Vendedores")
                DS = BaseII.ConsultaDS("GraficaVentas_4", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 15 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Del Depto. de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Del Depto. de Ventas de Servicios de " & eServicio
                Else
                    eServicio = "Gr�fica Del Depto. de Ventas de Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_5.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(3, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("GraficaVentas_5")
                listatablas.Add("Servicios")
                DS = BaseII.ConsultaDS("GraficaVentas_5", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 16 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales con Servicios de" & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales con Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_6.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(3, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("GraficaVentas_6")
                listatablas.Add("SUCURSALES")
                DS = BaseII.ConsultaDS("GraficaVentas_6", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 20 Then

                Titulo = "Detalle de Vendedores del Servicio " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte - " & Titulo
                End If
                reportPath = RutaReportes + "\ReportMetasVen.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasVen")
                DS = BaseII.ConsultaDS("ReporteMetasVen", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 21 Then
                Titulo = "Detalle de Sucursales del Servicio " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte - " & Titulo
                End If

                reportPath = RutaReportes + "\ReportMetasSuc.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasSuc")
                DS = BaseII.ConsultaDS("ReporteMetasSuc", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 22 Then
                Titulo = "Gr�fica de Medidores de Vendedores del Servicio" & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores de Vendedores"
                End If

                reportPath = RutaReportes + "\ReportMetasVenGraf.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasVenGraf")
                DS = BaseII.ConsultaDS("ReporteMetasVenGraf", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 23 Then
                Titulo = "Gr�fica de Medidores de Sucursales del Servicio" & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores de Sucursales"
                End If

                reportPath = RutaReportes + "\ReportMetasSucGraf.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasSucGraf")
                DS = BaseII.ConsultaDS("ReporteMetasSucGraf", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 30 Then
                Titulo = "Medidores de Ingresos"

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidores de Ingresos"
                End If


                reportPath = RutaReportes + "\ReportMetasIng.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasIng")
                DS = BaseII.ConsultaDS("ReporteMetasIng", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 31 Then
                Titulo = "Gr�fica Medidores de Ingresos del Servicio de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica Medidores de Ingresos"
                End If

                reportPath = RutaReportes + "\ReportMetasIngGraf.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasIngGraf")
                DS = BaseII.ConsultaDS("ReporteMetasIngGraf", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 32 Then
                Titulo = "Reporte de Ingresos Por Punto de Cobro"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Ingresos Por Punto de Cobro"
                End If

                reportPath = RutaReportes + "\ReportMetasIngSuc.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasIngSuc")
                DS = BaseII.ConsultaDS("ReporteMetasIngSuc", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 33 Then
                Titulo = "Gr�fica de Ingresos Por Punto de Cobro del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Ingresos Por Punto de Cobro"
                End If

                reportPath = RutaReportes + "\ReportMetasIngSucGraf.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasIngSucGraf")
                DS = BaseII.ConsultaDS("ReporteMetasIngSucGraf", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 34 Then
                Titulo = "Reporte Medidores de Cartera"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Medidores de Cartera"
                End If

                reportPath = RutaReportes + "\ReportMetasCartera.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)


                ''@MesIni
                'customersByCityReport.SetParameterValue(0, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(1, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(2, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(3, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CObj(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CObj(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CObj(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CObj(eAnioFin))

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteMetasCartera")

                DS = BaseII.ConsultaDS("ReporteMetasCartera", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 40 Then
                Titulo = "Clientes con Contrato a Plazo Forzoso de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = Titulo
                End If

                'reportPath = RutaReportes + "\ReportContratoF.rpt"
                reportPath = RutaReportes + "\ReportPlazoForzoso.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Orden
                'customersByCityReport.SetParameterValue(1, eOpContratoF)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)

                'eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 41 Then
                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(3, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(4, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(5, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(6, eAnioFin)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(7, "")
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupo1")
                DS = BaseII.ConsultaDS("ReporteMetasGrupo1", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 42 Then

                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo2.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(3, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(4, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(5, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(6, eAnioFin)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(7, "")
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupo2")
                DS = BaseII.ConsultaDS("ReporteMetasGrupo2", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 43 Then

                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoN.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(3, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(4, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(5, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(6, eAnioFin)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(7, "")
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupoN")
                DS = BaseII.ConsultaDS("ReporteMetasGrupoN", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 44 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1Graf.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(3, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(4, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(5, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(6, eAnioFin)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(7, "")
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupo1Graf")
                DS = BaseII.ConsultaDS("ReporteMetasGrupo1Graf", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 45 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo2Graf.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(3, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(4, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(5, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(6, eAnioFin)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(7, "")
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupo2Graf")
                DS = BaseII.ConsultaDS("ReporteMetasGrupo2Graf", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 46 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoNGraf.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@MesIni
                'customersByCityReport.SetParameterValue(3, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(4, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(5, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(6, eAnioFin)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(7, "")
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@MesIni", SqlDbType.Int, CInt(eMesIni))
                BaseII.CreateMyParameter("@AnioIni", SqlDbType.Int, CInt(eAnioIni))
                BaseII.CreateMyParameter("@MesFin", SqlDbType.Int, CInt(eMesFin))
                BaseII.CreateMyParameter("@AnioFin", SqlDbType.Int, CInt(eAnioFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupoNGraf")
                DS = BaseII.ConsultaDS("ReporteMetasGrupoNGraf", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 47 Then

                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1Det.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(3, "")
                ''@FechaIni
                'customersByCityReport.SetParameterValue(4, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(5, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupo1Det")
                DS = BaseII.ConsultaDS("ReporteMetasGrupo1Det", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 48 Then

                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoNDet.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(2, eClv_Session)
                ''@TipoGrupo
                'customersByCityReport.SetParameterValue(3, "")
                ''@FechaIni
                'customersByCityReport.SetParameterValue(4, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(5, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@TipoMeta", SqlDbType.VarChar, "")

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupoNDet")
                DS = BaseII.ConsultaDS("ReporteMetasGrupoNDet", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 49 Then
                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportComisionesN.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@FechaIni
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(3, eFechaFin)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ComisionesVendedoresN")
                DS = BaseII.ConsultaDS("ComisionesVendedoresN", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 50 Then
                Titulo = "Gr�fica de Atenci�n Telef�nica por Servicios al Cliente de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf50.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@FechaIni
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(3, eFechaFin)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReportAtenTelGraf50")
                DS = BaseII.ConsultaDS("ReportAtenTelGraf50", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 51 Then
                Titulo = "Gr�fica de Atenci�n Telef�nica por Usuarios y Servicios al Cliente de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf51.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReportAtenTelGraf51")
                DS = BaseII.ConsultaDS("ReportAtenTelGraf51", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@FechaIni
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 52 Then
                Titulo = "Gr�fica de Llamadas de Atenci�n Telef�nica del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf52.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@FechaIni
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(3, eFechaFin)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReportAtenTelGraf52")
                DS = BaseII.ConsultaDS("ReportAtenTelGraf52", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 53 Then
                Titulo = "Reporte de Status de Ventas de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If


                reportPath = RutaReportes + "\ReportStatusVentasN.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                ''Clv_Grupo
                'customersByCityReport.SetParameterValue(0, eClv_Grupo)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(1, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(2, eFechaFin)
                ''@ServicioPadre
                'customersByCityReport.SetParameterValue(3, eTipSer)
                ''@Contratado
                'customersByCityReport.SetParameterValue(4, eCont)
                ''@Instalado
                'customersByCityReport.SetParameterValue(5, eInst)
                ''@Desconectado
                'customersByCityReport.SetParameterValue(6, eDesc)
                ''@Suspendido
                'customersByCityReport.SetParameterValue(7, eSusp)
                ''@Baja
                'customersByCityReport.SetParameterValue(8, eBaja)
                ''@Fuera de Area
                'customersByCityReport.SetParameterValue(9, eFuera)
                ''@Temporal
                'customersByCityReport.SetParameterValue(10, eTempo)
                ''@Clv_SessionVendedores
                'customersByCityReport.SetParameterValue(11, eClv_Session)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CInt(eClv_Grupo))
                BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@ServicioPadre", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Cont", SqlDbType.Int, CInt(eCont))
                BaseII.CreateMyParameter("@Inst", SqlDbType.Int, CInt(eInst))
                BaseII.CreateMyParameter("@Desc", SqlDbType.Int, CInt(eDesc))
                BaseII.CreateMyParameter("@Susp", SqlDbType.Int, CInt(eSusp))
                BaseII.CreateMyParameter("@Baja", SqlDbType.Int, CInt(eBaja))
                BaseII.CreateMyParameter("@Fuera", SqlDbType.Int, CInt(eFuera))
                BaseII.CreateMyParameter("@Tem", SqlDbType.Int, CInt(eTempo))
                BaseII.CreateMyParameter("@Clv_SessionUsuarios", SqlDbType.BigInt, CInt(eClv_Session))

                Dim listatablas As New List(Of String)
                listatablas.Add("StatusDeVentasN")
                DS = BaseII.ConsultaDS("StatusDeVentasN", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)



                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 54 Then
                Titulo = "Reporte de PPV"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de PPV"
                End If


                reportPath = RutaReportes + "\ReportPPV.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                'Contrato
                customersByCityReport.SetParameterValue(0, eContrato)
                '@FechaIni
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(2, eFechaFin)
                '@Res
                customersByCityReport.SetParameterValue(3, 0)
                '@OP1
                customersByCityReport.SetParameterValue(4, eOP1)
                '@OP2
                customersByCityReport.SetParameterValue(5, eOP2)
                '@OP3
                customersByCityReport.SetParameterValue(6, eOP3)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 55 Then
                Titulo = "Reporte de Servicios Contratados"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Servicios Contratados"
                End If


                reportPath = RutaReportes + "\ReportVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 56 Then
                Titulo = "Reporte de Ventas en Baja de todos los Grupos de Ventas"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Ventas en Baja"
                End If


                reportPath = RutaReportes + "\ReportVentasBaja.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 57 Then
                Titulo = "Bit�cora de Correo(s)"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Bit�cora de Correo(s)"
                End If


                reportPath = RutaReportes + "\ReportCorreo.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clave
                'customersByCityReport.SetParameterValue(0, eClaveCorreo)
                ''@FechaIni
                'customersByCityReport.SetParameterValue(1, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(2, eFechaFin)
                ''@Op
                'customersByCityReport.SetParameterValue(3, eOpCorreo)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, CInt(eClaveCorreo))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, CInt(eOpCorreo))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReportCorreo")
                DS = BaseII.ConsultaDS("ReportCorreo", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 58 Then
                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportComisiones0.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Session
                'customersByCityReport.SetParameterValue(0, eClv_Session)
                ''@FechaIni
                'customersByCityReport.SetParameterValue(1, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(2, eFechaFin)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ComisionesVendedores0")
                DS = BaseII.ConsultaDS("ComisionesVendedores0", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 59 Then
                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo0Det.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)


                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Clv_Session
                'customersByCityReport.SetParameterValue(1, eClv_Session)
                ''@FechaIni
                'customersByCityReport.SetParameterValue(2, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(3, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, CInt(eClv_Session))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMetasGrupo0Det")
                DS = BaseII.ConsultaDS("ReporteMetasGrupo0Det", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 60 Then
                Titulo = "Reporte de Status de Ventas de " & eGrupo

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If


                reportPath = RutaReportes + "\ReportStatusVentas0.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(0, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(1, eFechaFin)
                ''@ServicioPadre
                'customersByCityReport.SetParameterValue(2, eTipSer)
                ''@Contratado
                'customersByCityReport.SetParameterValue(3, eCont)
                ''@Instalado
                'customersByCityReport.SetParameterValue(4, eInst)
                ''@Desconectado
                'customersByCityReport.SetParameterValue(5, eDesc)
                ''@Suspendido
                'customersByCityReport.SetParameterValue(6, eSusp)
                ''@Baja
                'customersByCityReport.SetParameterValue(7, eBaja)
                ''@Fuera de Area
                'customersByCityReport.SetParameterValue(8, eFuera)
                ''@Temporal
                'customersByCityReport.SetParameterValue(9, eTempo)
                ''@Clv_SessionVendedores
                'customersByCityReport.SetParameterValue(10, eClv_Session)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@ServicioPadre", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@Cont", SqlDbType.Int, CInt(eCont))
                BaseII.CreateMyParameter("@Inst", SqlDbType.Int, CInt(eInst))
                BaseII.CreateMyParameter("@Desc", SqlDbType.Int, CInt(eDesc))
                BaseII.CreateMyParameter("@Susp", SqlDbType.Int, CInt(eSusp))
                BaseII.CreateMyParameter("@Baja", SqlDbType.Int, CInt(eBaja))
                BaseII.CreateMyParameter("@Fuera", SqlDbType.Int, CInt(eFuera))
                BaseII.CreateMyParameter("@Tem", SqlDbType.Int, CInt(eTempo))
                BaseII.CreateMyParameter("@Clv_SessionUsuarios", SqlDbType.BigInt, CInt(eClv_Session))

                Dim listatablas As New List(Of String)
                listatablas.Add("StatusDeVentas0")
                DS = BaseII.ConsultaDS("StatusDeVentas0", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 61 Then
                Titulo = "Reporte Ventas Totales de " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Ventas Totales"
                End If


                reportPath = RutaReportes + "\ReportVentasTotales.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@ServicioPadre
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 62 Then


                Titulo = "Reporte de Interfaz " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Interfaz"
                End If


                reportPath = RutaReportes + "\ReportCNR_CNRDig.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@ServicioPadre
                'customersByCityReport.SetParameterValue(0, eOpCNRCNRDIG)
                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(1, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(2, eFechaFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, CShort(eOpCNRCNRDIG))
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteCNR_CNRDig")

                DS = BaseII.ConsultaDS("ReporteCNR_CNRDig", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 63 Then
                Titulo = "Reporte de Resumen de Clientes"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Resumen de Clientes"
                End If


                reportPath = RutaReportes + "\ReportResumenDeClientes.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteResumenDeClientes")

                DS = BaseII.ConsultaDS("ReporteResumenDeClientes", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 64 Then
                Titulo = "Reporte Auxiliar"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Auxiliar"
                End If


                reportPath = RutaReportes + "\ReportTelefonia.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 65 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Hoteles con Fecha de �ltimo Pago"
                End If


                reportPath = RutaReportes + "\ReportHoteles.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteHoteles")

                DS = BaseII.ConsultaDS("ReporteHoteles", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 66 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte De Ordenes"
                End If


                reportPath = RutaReportes + "\ReportOrdenes.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Fecha_Ini
                'customersByCityReport.SetParameterValue(0, eFechaIni)
                ''@Fecha_Fin
                'customersByCityReport.SetParameterValue(1, eFechaFin)
                ''@Op
                'customersByCityReport.SetParameterValue(2, eOp)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFin))
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, CShort(eOp))

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteOrdenes")

                DS = BaseII.ConsultaDS("ReporteOrdenes", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 67 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Gerencial"
                End If

                reportPath = RutaReportes + "\ReportGerencial.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteGerencial")

                DS = BaseII.ConsultaDS("ReporteGerencial", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 68 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Resumen de Cancelaciones"
                End If

                reportPath = RutaReportes + "\ReportCancelaciones.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 69 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Permanencia"
                End If

                reportPath = RutaReportes + "\ReportPermanencia.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Session
                'customersByCityReport.SetParameterValue(0, eClv_Session)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CObj(eClv_Session))
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CObj(eTipSer))
                BaseII.CreateMyParameter("@MESINI", SqlDbType.Int, CObj(eMesIni))
                BaseII.CreateMyParameter("@ANIOINI", SqlDbType.Int, CObj(eAnioIni))
                BaseII.CreateMyParameter("@MESFIN", SqlDbType.Int, CObj(eMesFin))
                BaseII.CreateMyParameter("@ANIOFIN", SqlDbType.Int, CObj(eAnioFin))

                Dim listatablas As New List(Of String)

                listatablas.Add("ReportePermanencia")

                DS = BaseII.ConsultaDS("ReportePermanencia", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"




            ElseIf eOpVentas = 70 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Resumen de Ventas por Status de " & eServicio
                End If

                reportPath = RutaReportes + "\ReportVentasPorStatus.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 71 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Valor de Cartera"
                End If

                reportPath = RutaReportes + "\ReportCarteraVsIngreso.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Op
                customersByCityReport.SetParameterValue(1, eBnd)

                eTituloComision = "Del " & eFechaIni
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

                'Dim conexion As New SqlConnection(MiConexion)

                'Dim cmd As New SqlCommand("ReporteCarteraVsIngreso", conexion)
                'cmd.CommandTimeout = 0
                'cmd.CommandType = CommandType.StoredProcedure

                'cmd.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = eFechaIni

                'Dim dataAdapter As New SqlDataAdapter(cmd)
                'Dim dataTable As New DataSet

                'dataAdapter.Fill(dataTable, "Consulta")



                'If Me.InvokeRequired Then
                '    Dim d As New Forms(AddressOf DelegadoForm)
                '    Me.Invoke(d, New Object() {Me, ""})
                'Else
                '    Me.Text = "Reporte de Valor de Cartera"
                'End If

                'reportPath = "\Reportes\ReportReportCarteraVsIngreso.rpt"

                'customersByCityReport.Load(reportPath)
                'customersByCityReport.SetDataSource(dataTable)



                ''SetDBLogonForReport(connectionInfo, customersByCityReport)

                '''@Fecha
                ''customersByCityReport.SetParameterValue(0, eFechaIni)

                ''eTituloComision = "Del " & eFechaIni
                ''customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 72 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Antig�edad de Cancelaciones"
                End If

                reportPath = RutaReportes + "\ReportAntiguedadDeCancelaciones.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Session
                'customersByCityReport.SetParameterValue(0, eClv_Session)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CObj(eClv_Session))
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CObj(eTipSer))
                BaseII.CreateMyParameter("@MESINI", SqlDbType.Int, CObj(eMesIni))
                BaseII.CreateMyParameter("@ANIOINI", SqlDbType.Int, CObj(eAnioIni))
                BaseII.CreateMyParameter("@MESFIN", SqlDbType.Int, CObj(eMesFin))
                BaseII.CreateMyParameter("@ANIOFIN", SqlDbType.Int, CObj(eAnioFin))

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteAntiguedadDeCancelaciones")

                DS = BaseII.ConsultaDS("ReporteAntiguedadDeCancelaciones", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 73 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Cancelaciones por Ejecutivo"
                End If

                reportPath = RutaReportes + "\ReportBajaDePrincipales.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 74 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Plazo Cubierto"
                End If

                reportPath = RutaReportes + "\ReportPlazoCubierto.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Session
                'customersByCityReport.SetParameterValue(0, eClv_Session)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Contratado
                'customersByCityReport.SetParameterValue(2, eC)
                ''@Instalado
                'customersByCityReport.SetParameterValue(3, eI)
                ''@Desconectado
                'customersByCityReport.SetParameterValue(4, eD)
                ''@Suspendido
                'customersByCityReport.SetParameterValue(5, eS)
                ''@Baja
                'customersByCityReport.SetParameterValue(6, eB)
                ''@Cubierto
                'customersByCityReport.SetParameterValue(7, eTipo)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CObj(eClv_Session))
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CObj(eTipSer))
                BaseII.CreateMyParameter("@CONTRATADO", SqlDbType.Bit, CObj(eC))
                BaseII.CreateMyParameter("@INSTALADO", SqlDbType.Bit, CObj(eI))
                BaseII.CreateMyParameter("@DESCONECTADO", SqlDbType.Bit, CObj(eD))
                BaseII.CreateMyParameter("@SUSPENDIDO", SqlDbType.Bit, CObj(eS))
                BaseII.CreateMyParameter("@BAJA", SqlDbType.Bit, CObj(eB))
                BaseII.CreateMyParameter("@CUBIERTO", SqlDbType.VarChar, CObj(eTipo))

                Dim listatablas As New List(Of String)

                listatablas.Add("ReportePlazoCubierto")

                DS = BaseII.ConsultaDS("ReportePlazoCubierto", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                'eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 75 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Adeudo Pagado"
                End If

                reportPath = RutaReportes + "\ReportAdeudoPagado.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Session
                'customersByCityReport.SetParameterValue(0, eClv_Session)
                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(1, eTipSer)
                ''@Pagado
                'customersByCityReport.SetParameterValue(2, eTipo)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CObj(eClv_Session))
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CObj(eTipSer))
                BaseII.CreateMyParameter("@PAGADO", SqlDbType.VarChar, CObj(eTipo))

                Dim listatablas As New List(Of String)

                listatablas.Add("ReporteAdeudoPagado")

                DS = BaseII.ConsultaDS("ReporteAdeudoPagado", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                'eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 76 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Encuestas"
                End If

                reportPath = RutaReportes + "\ReportEncuestas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@IDEncuesta
                customersByCityReport.SetParameterValue(0, eIDEncuesta)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 77 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Recontrataciones"
                End If

                reportPath = RutaReportes + "\ReportRecontratacion.rpt"

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, CObj(eFechaIni))
                BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, CObj(eFechaFin))

                Dim Lt As New List(Of String)
                Lt.Add("ReporteRecontrataciones")

                DS = BaseII.ConsultaDS("ReporteRecontrataciones", Lt)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@FechaIni
                'customersByCityReport.SetParameterValue(0, eFechaIni)
                ''@FechaFin
                'customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

                'CrystalReportViewer1.ReportSource = customersByCityReport
                'CrystalReportViewer1.Zoom(75)

            ElseIf eOpVentas = 78 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Clientes con Pagos Diferidos"
                End If

                reportPath = RutaReportes + "\ReportPagosDif.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Primer
                'customersByCityReport.SetParameterValue(1, ePrimero)
                ''@Segundo
                'customersByCityReport.SetParameterValue(2, eSegundo)
                ''@Instalados
                'customersByCityReport.SetParameterValue(3, eInst)
                ''@Suspensi�n Temporal
                'customersByCityReport.SetParameterValue(4, eTempo)

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(eTipSer))
                BaseII.CreateMyParameter("@PERIODO1", SqlDbType.Bit, CByte(ePrimero))
                BaseII.CreateMyParameter("@PERIODO2", SqlDbType.Bit, CByte(eSegundo))
                BaseII.CreateMyParameter("@I", SqlDbType.Bit, CByte(eInst))
                BaseII.CreateMyParameter("@T", SqlDbType.Bit, CByte(eTempo))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReportePagosDif")

                DS = BaseII.ConsultaDS("ReportePagosDif", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 79 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de n�mero/tipo de Decodificadores por Cliente"
                End If

                reportPath = RutaReportes + "\ReportDecodificadores.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_Session
                'customersByCityReport.SetParameterValue(0, LocClv_session)
                ''Cont
                'customersByCityReport.SetParameterValue(1, eC)
                ''Inst
                'customersByCityReport.SetParameterValue(2, eI)
                ''Desc
                'customersByCityReport.SetParameterValue(3, eD)
                ''Susp
                'customersByCityReport.SetParameterValue(4, eS)
                ''Baja
                'customersByCityReport.SetParameterValue(5, eB)
                ''Fuera
                'customersByCityReport.SetParameterValue(6, eF)
                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CInt(LocClv_session))
                BaseII.CreateMyParameter("@C", SqlDbType.Bit, CByte(eC))
                BaseII.CreateMyParameter("@I", SqlDbType.Bit, CByte(eI))
                BaseII.CreateMyParameter("@D", SqlDbType.Bit, CByte(eD))
                BaseII.CreateMyParameter("@S", SqlDbType.Bit, CByte(eS))
                BaseII.CreateMyParameter("@B", SqlDbType.Bit, CByte(eB))
                BaseII.CreateMyParameter("@F", SqlDbType.Bit, CByte(eF))

                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteDecodificadores")

                DS = BaseII.ConsultaDS("ReporteDecodificadores", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)



                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 80 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Valor de Cartera"
                End If

                reportPath = RutaReportes + "\ReportCarteraVsIngresoDet.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Op
                customersByCityReport.SetParameterValue(1, eBnd)
                '@Op
                customersByCityReport.SetParameterValue(2, LocClv_session)


                eTituloComision = "Del " & eFechaIni
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 81 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Retiro de Aparatos"
                End If

                Dim sBuilder As New StringBuilder("EXEC ReporteRetiroAparatos " + eMesIni.ToString() + ", " + eAnioIni.ToString() + ", " + eMesFin.ToString() + ", " + eAnioFin.ToString() + ", '" + eTrabajo + "'")
                Dim dAdapter As New SqlDataAdapter(sBuilder.ToString, MiConexion)
                Dim dSet As New DataSet

                Try
                    dAdapter.Fill(dSet)
                    dSet.Tables(0).TableName = "ReporteRetiroAparatos"
                    dSet.Tables(1).TableName = "DetReporteRetiroAparatos"

                    reportPath = RutaReportes + "\ReporteRetiroAparatos.rpt"
                    customersByCityReport.Load(reportPath)
                    customersByCityReport.SetDataSource(dSet)

                    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
                    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

                Catch ex As Exception

                End Try
            ElseIf eOpVentas = 95 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Repote de Pruebas de Internet"
                End If

                Dim dSet As New DataSet
                dSet = REPORTEPruebaInternet(eFechaIni, eFechaFin)
                reportPath = RutaReportes + "\REPORTEPruebaInternet.rpt"
                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(dSet)

            ElseIf eOpVentas = 96 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Listados de Clientes por Sector y Poste"
                End If

                Dim dSet As New DataSet
                dSet = REPORTEClientesClaveTecnica(eClv_Sector, eIdPoste)
                reportPath = RutaReportes + "\REPORTEClientesClaveTecnica.rpt"
                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(dSet)

            ElseIf eOpVentas = 97 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Listado de Cobros a Domicilio"
                End If

                Dim dSet As New DataSet
                dSet = REPORTEVentasCobroADomicilio(eFechaIni, eFechaFin)
                reportPath = RutaReportes + "\REPORTEVentasCobroADomicilio.rpt"
                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(dSet)

            ElseIf eOpVentas = 98 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Listado de Comisiones de Cobros a Domicilio"
                End If

                Dim dSet As New DataSet
                dSet = REPORTEComisionesCobroADomicilio(eFechaIni, eFechaFin)
                reportPath = RutaReportes + "\REPORTEComisionesCobroADomicilio.rpt"
                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(dSet)

            ElseIf eOpVentas = 99 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Listado de Promesas de Pago"
                End If

                Dim dSet As New DataSet
                dSet = REPORTEProrroga(OpProrroga, eFechaIni, eFechaFin)
                reportPath = RutaReportes + "\REPORTEProrroga.rpt"
                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(dSet)


                'Yahve -------------------------------
            ElseIf eOpVentas = 100 Then
                Titulo = "Resumen de Puntos de Antig�edad"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Resumen de Puntos de Antig�edad "
                End If


                reportPath = RutaReportes + "\RepAntiguedadResumen.rpt"

                'BaseII.limpiaParametros()
                'BaseII.CreateMyParameter("")
                'customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_inicial
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Final
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & eTituloComision & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 101 Then
                Titulo = "Reporte de Mensualidades"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Mensualidades anteriores y actuales. "
                End If


                reportPath = RutaReportes + "\ReporteMensualidad.rpt"

                Dim DS As New DataSet
                DS.Clear()
                BaseII.limpiaParametros()


                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteMensualidad")

                DS = BaseII.ConsultaDS("ReporteMensualidades", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)



                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If


            If eBndGraf = True Or eOpVentas = 20 Or eOpVentas = 21 Or eOpVentas = 22 Or eOpVentas = 23 Or eOpVentas = 31 Or eOpVentas = 33 Or eOpVentas = 57 Then
                eBndGraf = False
                eBndVen = True
                Me.customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            End If

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If

            eOpPPE = 0
            eOpVentas = 0
            eOpCNRCNRDIG = 0

            customersByCityReport = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
            'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmImprimirComision_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PantallaProcesando.Show()
        Me.BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        ConfigureCrystalReports()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub

    Private Function REPORTEProrroga(ByVal Op As Integer, ByVal FECHAINI As DateTime, ByVal FECHAFIN As DateTime) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEProrroga")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, FECHAINI)
        BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, FECHAFIN)
        Return BaseII.ConsultaDS("REPORTEProrroga", tableNameList)
    End Function

    Private Function REPORTEVentasCobroADomicilio(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEVentasCobroADomicilio")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
        Return BaseII.ConsultaDS("REPORTEVentasCobroADomicilio", tableNameList)
    End Function

    Private Function REPORTEComisionesCobroADomicilio(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEComisionesCobroADomicilio")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
        Return BaseII.ConsultaDS("REPORTEComisionesCobroADomicilio", tableNameList)
    End Function

    Private Function REPORTEClientesClaveTecnica(ByVal CLV_SECTOR As Integer, ByVal IDPOSTE As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEClientesClaveTecnica")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SECTOR", SqlDbType.Int, CLV_SECTOR)
        BaseII.CreateMyParameter("@IDPOSTE", SqlDbType.Int, IDPOSTE)
        Return BaseII.ConsultaDS("REPORTEClientesClaveTecnica", tableNameList)
    End Function

    Private Function REPORTEPruebaInternet(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEPruebaInternet")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
        Return BaseII.ConsultaDS("REPORTEPruebaInternet", tableNameList)
    End Function

End Class