﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRegresaAparatos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Cablemodem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mac = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Orden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEstadoAparato = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Aparato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRecibio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRegresarAlmacen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colTipoAparato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.btnRegresarAlmacen = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Clv_Cablemodem, Me.Mac, Me.Clv_Orden, Me.colEstadoAparato, Me.Aparato, Me.colRecibio, Me.colRegresarAlmacen, Me.colTipoAparato})
        Me.DataGridView1.Location = New System.Drawing.Point(12, 12)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(992, 632)
        Me.DataGridView1.TabIndex = 0
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID.DefaultCellStyle = DataGridViewCellStyle2
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'Clv_Cablemodem
        '
        Me.Clv_Cablemodem.DataPropertyName = "CLV_CABLEMODEM"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Cablemodem.DefaultCellStyle = DataGridViewCellStyle3
        Me.Clv_Cablemodem.HeaderText = "Clave Aparato"
        Me.Clv_Cablemodem.Name = "Clv_Cablemodem"
        Me.Clv_Cablemodem.ReadOnly = True
        Me.Clv_Cablemodem.Width = 130
        '
        'Mac
        '
        Me.Mac.DataPropertyName = "MAC"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mac.DefaultCellStyle = DataGridViewCellStyle4
        Me.Mac.HeaderText = "Mac ó No. Serie"
        Me.Mac.Name = "Mac"
        Me.Mac.ReadOnly = True
        Me.Mac.Width = 170
        '
        'Clv_Orden
        '
        Me.Clv_Orden.DataPropertyName = "CLV_ORDEN"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Orden.DefaultCellStyle = DataGridViewCellStyle5
        Me.Clv_Orden.HeaderText = "No. Orden"
        Me.Clv_Orden.Name = "Clv_Orden"
        Me.Clv_Orden.ReadOnly = True
        Me.Clv_Orden.Width = 140
        '
        'colEstadoAparato
        '
        Me.colEstadoAparato.DataPropertyName = "EstadoAparato"
        Me.colEstadoAparato.HeaderText = "Buen Estado"
        Me.colEstadoAparato.Name = "colEstadoAparato"
        '
        'Aparato
        '
        Me.Aparato.DataPropertyName = "APARATO"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aparato.DefaultCellStyle = DataGridViewCellStyle6
        Me.Aparato.HeaderText = "Tipo"
        Me.Aparato.Name = "Aparato"
        Me.Aparato.ReadOnly = True
        Me.Aparato.Width = 140
        '
        'colRecibio
        '
        Me.colRecibio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colRecibio.DataPropertyName = "RECIBIO"
        Me.colRecibio.HeaderText = "Recibió"
        Me.colRecibio.Name = "colRecibio"
        Me.colRecibio.ReadOnly = True
        '
        'colRegresarAlmacen
        '
        Me.colRegresarAlmacen.DataPropertyName = "RegresarAlmacen"
        Me.colRegresarAlmacen.HeaderText = "Regresar Almacen"
        Me.colRegresarAlmacen.Name = "colRegresarAlmacen"
        '
        'colTipoAparato
        '
        Me.colTipoAparato.DataPropertyName = "TipoAparato"
        Me.colTipoAparato.HeaderText = "Tipo Aparato"
        Me.colTipoAparato.Name = "colTipoAparato"
        Me.colTipoAparato.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(868, 650)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 30
        Me.Button5.Text = "&CERRAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(721, 650)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "&IMPRIMIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'btnRegresarAlmacen
        '
        Me.btnRegresarAlmacen.BackColor = System.Drawing.Color.DarkOrange
        Me.btnRegresarAlmacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegresarAlmacen.Location = New System.Drawing.Point(470, 650)
        Me.btnRegresarAlmacen.Name = "btnRegresarAlmacen"
        Me.btnRegresarAlmacen.Size = New System.Drawing.Size(236, 36)
        Me.btnRegresarAlmacen.TabIndex = 32
        Me.btnRegresarAlmacen.Text = "&Regresar al Almacen"
        Me.btnRegresarAlmacen.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'FrmRegresaAparatos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.btnRegresarAlmacen)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.DataGridView1)
        Me.MaximizeBox = False
        Me.Name = "FrmRegresaAparatos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regresas Aparatos al Almacen"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents btnRegresarAlmacen As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Cablemodem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mac As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Orden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEstadoAparato As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Aparato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRecibio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRegresarAlmacen As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colTipoAparato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
