Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic

Public Class FrmImprimirPPE
    Private customersByCityReport As ReportDocument

    Private Sub ConfigureCrystalReports()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim eFecha As String = Nothing
        eFecha = "Del " & eFechaIniPPE & " al " & eFechaFinPPE
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing


        If eOpPPE = 1 Then

            Me.Text = "Reporte de Venta de Pel�culas PPE"


            reportPath = RutaReportes + "\ReportServiciosPPE.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(1, eFechaFinPPE)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"


        ElseIf eOpPPE = 2 Then
            Me.Text = "Bit�cora de Activaci�n de Paquetes"


            reportPath = RutaReportes + "\ReportBitActPaq.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(1, eFechaFinPPE)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FechaInicial", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@FechaFinal", SqlDbType.DateTime, CObj(eFechaFinPPE))

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteBitActPaq")
            DS = BaseII.ConsultaDS("ReporteBitActPaq", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"




        ElseIf eOpPPE = 3 Then

            Me.Text = "Reporte Resumen de Ventas Sucursal"

            Dim conexion As New SqlConnection(MiConexion)

            Dim sBuilder As New StringBuilder("EXEC ReporteResumenVentas '" + eFechaIniPPE + "', '" + eFechaFinPPE + "'")
            'Dim sBuilder As New StringBuilder("EXEC REPORTETEST")
            Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
            Dim dTable As New DataTable

            dAdapter.Fill(dTable)

            reportPath = RutaReportes + "\ReportResumenVentas.rpt"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dTable)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"




        ElseIf eOpPPE = 4 Then
            Me.Text = "Reporte Resumen de Ventas Vendedores"


            reportPath = RutaReportes + "\ReportResumenVentasVendedores.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(1, eFechaFinPPE)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(eFechaFinPPE))

            Dim listatablas As New List(Of String)
            listatablas.Add("VentasVendedor")
            DS = BaseII.ConsultaDS("VentasVendedor", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
        ElseIf eOpPPE = 5 Then
            eOpPPE = 0
            Me.Text = "Listado de Adelantados"
            Dim dSet As New DataSet
            dSet = LISTADOAdelantados()
            customersByCityReport.Load(RutaReportes + "\LISTADOAdelantados.rpt")
            customersByCityReport.SetDataSource(dSet)
        ElseIf eOpPPE = 6 Then
            Me.Text = "Resumen Ordenes y Quejas Ejecutadas"
            reportPath = RutaReportes + "\ResumenOrdenQueja.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, CObj(eFechaFinPPE))

            Dim listatablas As New List(Of String)
            listatablas.Add("UspReporteResumenOrdenQueja")
            DS = BaseII.ConsultaDS("ReporteResumenOrdenQueja", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloCiudad & "'"
        ElseIf eOpPPE = 7 Then
            Me.Text = "Resumen Ordenes y Quejas Ejecutadas"
            reportPath = RutaReportes + "\ResumenOrdenQueja.rpt"
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@fechaini", SqlDbType.DateTime, CObj(eFechaIniPPE))
            BaseII.CreateMyParameter("@fechafin", SqlDbType.DateTime, CObj(eFechaFinPPE))

            Dim listatablas As New List(Of String)
            listatablas.Add("UspReporteResumenOrdenQueja")
            DS = BaseII.ConsultaDS("ReporteResumenOrdenQueja", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + eFechaIniPPE.Date + " A la Fecha: " + eFechaFinPPE.Date
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloCiudad & "'"
        ElseIf eOpPPE = 8 Then
            Me.Text = "Resumen Detallado Trabajos"
            Dim mySelectFormula = "Reporte Resumen Trabajos Detallado"
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            tableNameList.Add("UspReporteResumenDetalladoTrabajo")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINICIAL", SqlDbType.DateTime, eFechaIniPPE)
            BaseII.CreateMyParameter("@FECHAFINAL", SqlDbType.DateTime, eFechaFinPPE)
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.Int, CLng(eClv_Session))

            dSet = BaseII.ConsultaDS("ReporteResumenDetalladoTrabajo", tableNameList)
            reportPath = RutaReportes + "\ReporteResumenTrabajosDetallado.rpt"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dSet)

            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
        End If






        Me.CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmImprimirPPE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ConfigureCrystalReports()
    End Sub

    Private Function LISTADOAdelantados() As DataSet
        BaseII.limpiaParametros()
        Dim tableNameList As New List(Of String)
        tableNameList.Add("LISTADOAdelantados")
        tableNameList.Add("GENERAL")
        Return BaseII.ConsultaDS("LISTADOAdelantados", tableNameList)
    End Function

End Class