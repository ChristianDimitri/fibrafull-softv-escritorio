﻿Imports System.Data.SqlClient

Public Class FrmRoboDeSeñal

    Private Sub ConRoboDeSeñalBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConRoboDeSeñalBindingNavigatorSaveItem.Click
        
        If Me.DescripcionTextBox.Text.Length < 0 Then
            MsgBox("Captura la Descripción.", , "Atención")
            Exit Sub
        End If
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConRoboDeSeñalTableAdapter.Connection = CON
            Me.ConRoboDeSeñalTableAdapter.Insert(eGloContrato, Me.DescripcionTextBox.Text)
            CON.Close()
            MsgBox(mensaje5)
            Me.Close()
        Catch
            MsgBox("Se ha producido un Error.")
        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConRoboDeSeñalTableAdapter.Connection = CON
            Me.ConRoboDeSeñalTableAdapter.Delete(eGloContrato)
            CON.Close()
            MsgBox(mensaje6)
            Me.Close()
        Catch
            MsgBox("Se ha producido un Error.")
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmRoboDeSeñal_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        eRobo = True
    End Sub

    Private Sub FrmRoboDeSeñal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        If eOpcion = "C" Then

            Me.ConRoboDeSeñalTableAdapter.Connection = CON
            Me.ConRoboDeSeñalTableAdapter.Fill(Me.DataSetEric.ConRoboDeSeñal, eGloContrato)

            Me.ConRoboDeSeñalBindingNavigator.Enabled = False
            'Me.DescripcionTextBox.Enabled = False
        End If

        If eOpcion = "M" Then
            Me.ConRoboDeSeñalTableAdapter.Connection = CON
            Me.ConRoboDeSeñalTableAdapter.Fill(Me.DataSetEric.ConRoboDeSeñal, eGloContrato)
            If Me.DescripcionTextBox.Text.Length < 0 Then
                Me.BindingNavigatorDeleteItem.Enabled = False
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class